const std = @import("std");
const prelude = @import("prelude.zig");

// Provides a simple abstraction that the kernel can use to write to
pub const KernelWriterBase = struct {
    context: usize,
    writeFn: *const fn (context: usize, bytes: []const u8) KernelWriteError!usize,
};

pub const KernelWriter = std.io.Writer(KernelWriterBase, KernelWriteError, kernelWrite);

pub const KernelWriteError = error{
    DeviceUnavailable,
    OtherError,
    WriterUninitialized,
};

pub var kout: KernelWriter = undefined;

fn kernelWrite(context: KernelWriterBase, bytes: []const u8) KernelWriteError!usize {
    return context.writeFn(context.context, bytes);
}

pub fn print(comptime fmt: []const u8, args: anytype) void {
    kout.print(fmt, args) catch return;
}

pub const LogLevel = enum {
    Debug,
    Info,
    Warn,
    Error,
};

pub fn filter(comptime level: LogLevel) bool {
    var max_level = @enumToInt(prelude.build_options.log_level);
    return @enumToInt(level) >= max_level;
}

pub fn log(comptime level: LogLevel, comptime fmt: []const u8, args: anytype) void {
    if (!filter(level)) {
        return;
    }
    print("[" ++ switch (level) {
        .Debug => "DEBUG",
        .Info => "INFO",
        .Warn => "WARN",
        .Error => "ERROR",
    } ++ "] " ++ fmt ++ "\n", args);
}

pub fn init(writer: KernelWriterBase) void {
    kout = KernelWriter{
        .context = writer,
    };
}
