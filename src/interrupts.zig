const arch = @import("prelude.zig").arch;
const std = @import("std");
// Interrupt handling

pub fn init() !void {
    arch.interrupts.os_interrupt_handler = interruptHandler;
}

pub fn interruptHandler(interrupt_frame: *arch.cpu.interrupts.InterruptFrame) void {
    if (interrupt_frame.int_no.isFault()) {
        faultHandler(interrupt_frame);
    } else {
        if (interrupt_frame.int_no == arch.cpu.interrupts.Fault.syscall) {
            syscallHandler(interrupt_frame);
        } else {
            userInterruptHandler(interrupt_frame);
        }
    }
}

pub fn faultHandler(interrupt_frame: *arch.cpu.interrupts.InterruptFrame) void {
    std.debug.panic("Fault: {}\n{}\n", .{ interrupt_frame.int_no, interrupt_frame });
}

pub fn syscallHandler(interrupt_frame: *arch.cpu.interrupts.InterruptFrame) void {
    std.debug.panic("Syscall: {})\n{}\n", .{ interrupt_frame.int_no, interrupt_frame });
}

pub fn userInterruptHandler(interrupt_frame: *arch.cpu.interrupts.InterruptFrame) void {
    std.debug.panic("User: {}\n{}\n", .{ interrupt_frame.int_no, interrupt_frame });
}
