BITS 64

;-------------------------------------------
; clear_interrupts()
; Should clear hardware interrupts of the CPU
;--------------------------------------------
[global clear_interrupts]
clear_interrupts:
    cli
    ret

;-----------------------------------------
; enable_interrupts()
; Should enable the interrupts of the CPU
;-----------------------------------------
[global enable_interrupts]
set_interrupts:
    sti
    ret

;-------------------------------
; halt()
; Should completely halt the CPU
;-------------------------------
[global halt]
halt:
    cli
    hlt
.infloop:
    jmp .infloop

[global getcr3]
getcr3:
    mov rax, cr3
    ret

[global setcr3]
setcr3:
    mov cr3, rdi
    ret

[global asm_invlpg]
asm_invlpg:
    invlpg [rdi]
    ret

; vim: ft=nasm
