[BITS 64]
[SECTION .text]

;;;;
; The C isr handler
extern _x86_64_isr_handler

;;;
; The current data segment we are using for the kernel
%define KERNEL_DATA_SEG (2 << 3)

; Some macros for pushing and popping all the registers of x86-64
; Although only used once, it is nice to do it this way to ensure that the stack between the two are the same
%macro pushaq 0
    push rax
    push rcx
    push rdx
    push rbx
    push rbp
    push rsi
    push rdi
    push r8
    push r9
    push r10
    push r11
    push r12
    push r13
    push r14
    push r15
%endmacro
%macro popaq 0
    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rdi
    pop rsi
    pop rbp
    pop rbx
    pop rdx
    pop rcx
    pop rax
%endmacro

;;;;;;;;;
; asm_lidt(*void)
; Loads an idt from a pointer to an IDTR
[global asm_lidt]
asm_lidt:
    lidt [rdi]
    ret

;;;
; isr_stub()
; called by the separate isr handlers. Gives a
; uniform method of handling an isr
isr_stub:
    ; Push all the 64-bit registers
    pushaq
    ; Push all the data segments (Stack alignment is kept because all segments add up to 64-bits here)
    mov ax, ds
    push ax
    mov ax, es
    push ax
    mov ax, fs
    push ax
    mov ax, gs
    push ax
    ; Load the kernel data segments
    mov ax, KERNEL_DATA_SEG
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    ; And we get into it
    mov rdi, rsp
    cld
    call _x86_64_isr_handler
    ; Pop all the data segments
    pop ax
    mov ax, gs
    pop ax
    mov ax, fs
    pop ax
    mov ax, es
    pop ax
    mov ax, ds
    ; And pop all the registers
    popaq
    add rsp, 16 ; Pops the error number off the stack
    ;add rsp, 16 ; Pops the interrupt number off the stack
    iretq

; We'll start by making all of our ISRs. From 0 to 255

; Make an ISR wrapper for each table
; Arg 1: ISR number
%macro isr_wrapper 1
isr_%1:
    ; Turn off interrupts
    cli
    ; Check if we have an error number with this ISR
    ; The ones with error numbers are:
    ; 8 - DF
    ; 10 -TS
    ; 11 - NP
    ; 12 - SS
    ; 13 - GP
    ; 14 - PF
    ; 17 - AC
    ; 21 - CP
    %ifn (%1 == 8) || ((%1 >= 10 ) && (%1 <= 14)) || (%1 == 17) || (%1 == 21)
        ; Push an error number of 0
        push 0
    %endif
    ; Push the interrupt number
    push %1
    jmp isr_stub ; And go to the isr_stub
%endmacro

; Create all 256 interrupt vectors.
%assign i 0
%rep 256
    isr_wrapper i
    %assign i i+1
%endrep

[section .rodata]

; void* isr_ptr_table[256];
; Holds all the pointers to the different isr vectors
[global isr_ptr_table]
isr_ptr_table:
%assign i 0
%rep 256
    dq isr_%+i
    %assign i i+1
%endrep

; vim: set asmsyntax=nasm
; vim: ft=nasm
