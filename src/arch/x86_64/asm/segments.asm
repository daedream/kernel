[BITS 64]
SECTION .text

;; asm_lgdt(*void);
; Loads a GDTR
[global asm_lgdt]
asm_lgdt:
    lgdt [rdi]
    ret

;; asm_load_cs(uint64_t);
[global asm_load_cs]
asm_load_cs:
    push rdi
    push $.next
    retfq
.next:
    ret

;; asm_load_ds(uint16_t);
[global asm_load_ds]
asm_load_ds:
    mov ds, di
    ret

;; asm_load_es(uint16_t);
[global asm_load_es]
asm_load_es:
    mov es, di
    ret

;; asm_load_fs(uint16_t);
[global asm_load_fs]
asm_load_fs:
    mov fs, di
    ret

;; asm_load_gs(uint16_t);
[global asm_load_gs]
asm_load_gs:
    mov gs, di
    ret

;; asm_load_ss(uint16_t);
[global asm_load_ss]
asm_load_ss:
    mov ss, di
    ret

; vim: set asmsyntax=nasm
; vim: ft=nasm
