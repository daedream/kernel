/// Implements a PageMapper for x86_64.
const std = @import("std");
const prelude = @import("../../prelude.zig");
const memory = prelude.arch.memory;
const mm = prelude.mm;
const pmm = mm.pmm;
const vmm = mm.vmm;
const PhysAddr = prelude.arch.memory.PhysAddr;
const VirtAddr = prelude.arch.memory.VirtAddr;
const PageMapper = vmm.PageMapper;

const vtable = PageMapper.VTable{
    .map = map,
    .mapRange = mapRange,
    .unmap = unmap,
    .unmapRange = unmapRange,
    .changeFlags = changeFlags,
    .changeRangeFlags = changeRangeFlags,
};

/// Initializes page mapping and returns a PageMapper for the virtual memory manager.
pub fn init() !PageMapper {
    return .{
        .ptr = undefined,
        .vtable = &vtable,
    };
}

fn convertFlags(flags: PageMapper.Flags) PageMapper.Error!memory.PageTableEntryFlags {
    var f: memory.PageTableEntryFlags = .{};
    f.no_execute = !flags.executable;
    f.writable = flags.write;
    f.user_accessable = flags.user;
    return f;
}

// TODO: Handle huge pages
fn getLastLevel(virtual_address: VirtAddr) PageMapper.Error!*memory.PageTableEntry {
    // We'll start by getting our top-level page table's address
    var table = memory.getLevel4PageTablePointer(mm.physical_offset);
    var level = @enumToInt(memory.PageTableLevel.four);
    while (level > 1) : (level -= 1) {
        //log.log(.Debug, "Current table (Level {})\n{}", .{ level, table });
        var index = memory.PageTableIndex.fromVirtAddr(virtual_address, @intToEnum(memory.PageTableLevel, level));
        var entry = table.getEntryAtIndex(index);
        if (entry.isUnused()) {
            var phys = pmm.allocator.allocHigh() catch {
                return error.OutOfMemory;
            };
            var virt_addr = phys.toVirtAddr(mm.physical_offset) catch {
                unreachable;
            };
            table = virt_addr.toPtr(*memory.PageTable);
            table.zero();
            entry.setPhysAddr(phys);
            entry.setFlags(.{
                .present = true,
                .writable = true,
                .user_accessable = true,
            });
        } else {}
        var virt_addr = entry.getPhysAddr().toVirtAddr(mm.physical_offset) catch {
            unreachable;
        };
        table = virt_addr.toPtr(*memory.PageTable);
    }
    {
        var index = memory.PageTableIndex.fromVirtAddr(virtual_address, memory.PageTableLevel.one);
        return table.getEntryAtIndex(index);
    }
}

fn map(ctx: *anyopaque, physical_address: PhysAddr, virtual_address: VirtAddr, flags: PageMapper.Flags) PageMapper.Error!void {
    _ = ctx;
    var entry = try getLastLevel(virtual_address);
    entry.setPhysAddr(physical_address);
    entry.setFlags(try convertFlags(flags));
    memory.invlpg(virtual_address);
    //memory.reloadCR3();
}

// TODO: Implement huge pages
fn mapRange(ctx: *anyopaque, physical_address: PhysAddr, virtual_address: VirtAddr, size: usize, flags: PageMapper.Flags) PageMapper.Error!void {
    var i: usize = 0;
    while (i < size / memory.page_size) : (i += 1) {
        return map(ctx, physical_address.add(i * memory.page_size), virtual_address.add(i * memory.page_size), flags);
    }
}

fn unmap(ctx: *anyopaque, virtual_address: VirtAddr) PageMapper.Error!void {
    _ = ctx;
    var entry = try getLastLevel(virtual_address);
    entry.setUnused();
    memory.invlpg(virtual_address);
    //memory.reloadCR3();
}

fn unmapRange(ctx: *anyopaque, virtual_address: VirtAddr, size: usize) PageMapper.Error!void {
    var i: usize = 0;
    while (i < size / memory.page_size) : (i += 1) {
        return unmap(ctx, virtual_address.add(i * memory.page_size));
    }
}

fn changeFlags(ctx: *anyopaque, virtual_address: VirtAddr, flags: PageMapper.Flags) PageMapper.Error!void {
    _ = ctx;
    var entry = try getLastLevel(virtual_address);
    entry.setFlags(try convertFlags(flags));
    memory.invlpg(virtual_address);
    //memory.reloadCR3();
}

fn changeRangeFlags(ctx: *anyopaque, virtual_address: VirtAddr, size: usize, flags: PageMapper.Flags) PageMapper.Error!void {
    var i: usize = 0;
    while (i < size / memory.page_size) : (i += 1) {
        return changeFlags(ctx, virtual_address.add(i * memory.page_size), flags);
    }
}
