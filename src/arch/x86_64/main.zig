pub const cpu = @import("cpu.zig");
/// Interrupt handling
pub const interrupts = @import("interrupts.zig");
pub const memory = @import("memory.zig");
pub const paging = @import("paging.zig");
pub const ports = @import("ports.zig");
pub const std = @import("std");
pub const log = @import("../../prelude.zig").log;
// Use this to ensure certain files are compiled
comptime {
    _ = @import("interrupts.zig");
}

pub const SerialInitError = error{
    SentinelByteIncorrect,
};

// This enum is required to have at least Any
pub const SerialPort = enum {
    Any,
    Com1,
    Com2,
    Com3,
    Com4,
    pub fn toPort(self: @This()) u16 {
        return switch (self) {
            .Any => 0x3f8,
            .Com1 => 0x3f8,
            .Com2 => 0x2f8,
            .Com3 => 0x3e8,
            .Com4 => 0x2e8,
        };
    }
};

pub const logging_port: SerialPort = .Com1;

pub const ArchError = error{Unknown};

// Called by kmain to initialize any sort of architecutre specific things
pub fn archInit() ArchError!void {
    cpu.interrupts.cli();
    try gdtInit();
    try idtInit();
}

// Define our static GDT entries
export var gdt_entries: [7]cpu.gdt.GdtEntry = undefined;
var gdtr: cpu.gdt.Gdtr = undefined;
pub fn gdtInit() ArchError!void {
    // Null entry required for the first one
    gdt_entries[0] = cpu.gdt.NullEntry;
    // Kernel code
    gdt_entries[1] = cpu.gdt.GdtEntry.newFlat(cpu.gdt.flags.access.basic_kernel | cpu.gdt.flags.access.executable, cpu.gdt.flags.limit.basic);
    // Kernel data
    gdt_entries[2] = cpu.gdt.GdtEntry.newFlat(cpu.gdt.flags.access.basic_kernel, cpu.gdt.flags.limit.basic);
    // User code
    gdt_entries[3] = cpu.gdt.GdtEntry.newFlat(cpu.gdt.flags.access.basic_user | cpu.gdt.flags.access.executable, cpu.gdt.flags.limit.basic);
    // User data
    gdt_entries[4] = cpu.gdt.GdtEntry.newFlat(cpu.gdt.flags.access.basic_user, cpu.gdt.flags.limit.basic);
    // These two will be used for TSS entries later on
    gdt_entries[5] = cpu.gdt.NullEntry;
    gdt_entries[6] = cpu.gdt.NullEntry;
    var gdt = gdt_entries[0..gdt_entries.len];
    gdtr = cpu.gdt.Gdtr.new(gdt);
    cpu.gdt.asm_lgdt(&gdtr);

    //var code_selector = cpu.Selector.toCodeSelector(.{ .ring = 0, .ldt = false, .index = 1 });
    var code_selector = cpu.Selector{
        .ring = 0,
        .ldt = false,
        .index = 1,
    };
    code_selector.load_cs();
    var data_selector = cpu.Selector{
        .ring = 0,
        .ldt = false,
        .index = 2,
    };
    data_selector.load_ds();
    data_selector.load_es();
    data_selector.load_fs();
    data_selector.load_gs();
    data_selector.load_ss();
}

extern var isr_ptr_table: [interrupts.idt_size]cpu.interrupts.IsrHandler;
pub fn idtInit() ArchError!void {
    var idt = interrupts.static_idt[0..interrupts.static_idt.len];
    for (idt) |*idt_entry, i| {
        idt_entry.* = cpu.idt.IdtEntry.new(isr_ptr_table[i], cpu.Selector{ .ring = 0, .ldt = false, .index = 1 }, cpu.idt.IdtEntry.IstIndex{ .index = 0 }, cpu.idt.IdtEntry.Attributes{
            .interrupt_trap = true,
            .ring = 0,
            .present = true,
        });
    }

    var idtr = cpu.idt.Idtr.new(idt);
    cpu.idt.asm_lidt(&idtr);
}

pub fn loggingInit() anyerror!log.KernelWriterBase {
    var serial_port = logging_port.toPort();
    const serial_baud_rate = 115200;

    const serial_divisor = 115200;
    ports.out(serial_port + 1, @as(u8, 0x00)); // Disable all inputs
    ports.out(serial_port + 3, @as(u8, 0x80)); // Set DLAB
    const divisor: u16 = @divFloor(serial_baud_rate, serial_divisor);
    ports.out(serial_port + 1, @intCast(u8, divisor & 0xFF)); // low byte
    ports.out(serial_port + 1, @intCast(u8, (divisor & 0xFF00) >> 8)); // high byte
    ports.out(serial_port + 3, @as(u8, 0x03)); // 8bn1
    ports.out(serial_port + 2, @as(u8, 0xC7)); // FIFO and clear
    ports.out(serial_port + 4, @as(u8, 0x0B)); // IRQs enabled

    // Make sure it works
    ports.out(serial_port + 4, @as(u8, 0x1E)); // Loopback mode
    ports.out(serial_port + 0, @as(u8, 0xAE)); // Sentinel byte
    if (ports.in(u8, serial_port + 0) != 0xAE) {
        return SerialInitError.SentinelByteIncorrect;
    }

    // Set it back to normal
    ports.out(serial_port + 4, @as(u8, 0x0F));
    return log.KernelWriterBase{
        .context = @as(usize, serial_port),
        .writeFn = serialWrite,
    };
}

pub fn loggingClose(base: log.KernelWriterBase) !void {
    _ = base;
}

pub fn serialWrite(context: usize, bytes: []const u8) log.KernelWriteError!usize {
    var i: usize = 0;
    for (bytes) |b| {
        i += 1;
        var port = @truncate(u16, context);
        try serialWriteByte(port, b);
    }
    return i;
}

pub fn serialWriteByte(port: u16, byte: u8) log.KernelWriteError!void {
    while (ports.in(u8, port + 5) & 0x20 == 0) {}

    ports.out(port, byte);
}

pub fn halt() noreturn {
    while (true) {
        asm volatile ("hlt");
    }
}

pub fn waitForInterrupt() void {
    asm volatile ("hlt");
}
