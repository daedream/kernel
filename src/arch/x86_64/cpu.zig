pub const std = @import("std");
pub const interrupts = struct {
    pub const InterruptFrame = packed struct {
        // Segment selectors
        gs: u16,
        fs: u16,
        es: u16,
        ds: u16,

        // Registers
        r15: u64,
        r14: u64,
        r13: u64,
        r12: u64,
        r11: u64,
        r10: u64,
        r9: u64,
        r8: u64,

        // General purpose registers

        rdi: u64,
        rsi: u64,
        rbp: u64,
        rbx: u64,
        rdx: u64,
        rcx: u64,
        rax: u64,

        // Interrupt information
        int_no: Fault,
        err_code: u64,

        rip: u64,
        cs: u64,
        rflags: u64,
        userrsp: u64,
        ss: u64,

        // To pretty print the frame
        pub fn format(value: InterruptFrame, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
            _ = try writer.write("InterruptFrame:\n");
            try writer.print("{} Error code: {}\n", .{ value.int_no, value.err_code });

            _ = try writer.write("Context:\n");
            try writer.print("rip: 0x{x}, cs: 0x{x}, rflags: 0x{x}\n", .{ value.rip, value.cs, value.rflags });
            try writer.print("userrsp: 0x{x}, ss: 0x{x}\n", .{ value.userrsp, value.ss });

            _ = try writer.write("Segments:\n");
            try writer.print("gs: 0x{x}, fs: 0x{x}, es: 0x{x}, ds: 0x{x}\n", .{ value.gs, value.fs, value.es, value.ds });

            _ = try writer.write("Registers:\n");
            try writer.print("rax: 0x{x}, rbx: 0x{x}, rcx: 0x{x}, rdx: 0x{x}\n", .{ value.rax, value.rbx, value.rcx, value.rdx });
            try writer.print("rbp: 0x{x}, rsi: 0x{x}, rdi: 0x{x}\n", .{ value.rbp, value.rsi, value.rdi });
            try writer.print("r8:  0x{x}, r9:  0x{x}, r10: 0x{x}, r11: 0x{x}\n", .{ value.r8, value.r9, value.r10, value.r11 });
            try writer.print("r12: 0x{x}, r13: 0x{x}, r14: 0x{x}, r15: 0x{x}\n", .{ value.r12, value.r13, value.r14, value.r15 });
        }
    };
    pub const Fault = enum(u64) {
        divide_error = 0,
        debug = 1,
        nmi = 2,
        breakpoint = 3,
        overflow = 4,
        bound_range_exceed = 5,
        invalid_opcode = 6,
        device_not_available = 7,
        double_fault = 8,
        invalid_tss = 10,
        segment_not_present = 11,
        stack_fault = 12,
        general_protection = 13,
        page_fault = 14,
        fpu_error = 16,
        alignment_check = 17,
        machine_check = 18,
        simd_floating_point = 19,
        virtualization = 20,
        control_protection = 21,
        user = 32,
        syscall = 255,
        _,
        pub fn getFaultName(fault: @This()) [*:0]const u8 {
            return switch (fault) {
                .divide_error => "Divide Error Exception",
                .debug => "Debug Exception",
                .nmi => "Non-Maskable Interrupt",
                .breakpoint => "Breakpoint Exception",
                .overflow => "Overflow Exception",
                .bound_range_exceed => "BOUND Range Exceed Exception",
                .invalid_opcode => "Invalid Opcode Exception",
                .device_not_available => "Device Not Available Exception",
                .double_fault => "Double Fault Exception",
                .invalid_tss => "Invalid TSS Exception",
                .segment_not_present => "Segment Not Present",
                .stack_fault => "Stack Fault Exception",
                .general_protection => "General Protection Exception",
                .page_fault => "Page Fault Exception",
                .fpu_error => "FPU Error",
                .alignment_check => "Alignment Check Exception",
                .machine_check => "Machine Check Exception",
                .simd_floating_point => "SIMD Floating Point Exception",
                .virtualization => "Virtualization Exception",
                .control_protection => "Control Protection Exception",
                .user => "User defined interrupt",
                .syscall => "Syscall interrupt",
                _ => "User defined interrupt",
            };
        }
        // Returns if this is a fault, or just a normal interrupt
        pub fn isFault(fault: @This()) bool {
            return @enumToInt(fault) < 32;
        }
        pub fn format(value: Fault, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
            try writer.print("Fault({}: {s})", .{ @enumToInt(value), getFaultName(value) });
        }
    };
    pub const IsrHandler = *const fn (*InterruptFrame) callconv(.C) void;

    pub fn int(comptime interrupt_no: u64) void {
        asm volatile ("int %[int_no]"
            :
            : [int_no] "i" (interrupt_no),
        );
    }

    pub fn cli() void {
        asm volatile ("cli");
    }
    pub fn sti() void {
        asm volatile ("sti");
    }
};

pub const idt = struct {
    pub const IdtEntry = packed struct {
        offset_low: u16,
        selector: Selector,
        ist_index: IstIndex,
        attributes: Attributes,
        offset_middle: u16,
        offset_high: u32,
        zero: u32 = 0,

        pub const IstIndex = packed struct {
            index: u3,
            zero: u5 = 0,
        };

        pub const Attributes = packed struct {
            interrupt_trap: bool,
            ones: u3 = 7,
            zero: u1 = 0,
            ring: u2,
            present: bool,
        };
        pub fn new(interrupt_vector: interrupts.IsrHandler, selector: Selector, ist: IdtEntry.IstIndex, attributes: Attributes) @This() {
            var interrupt_vector_val: u64 = @intCast(u64, @ptrToInt(interrupt_vector));
            return @This(){
                .offset_low = @intCast(u16, interrupt_vector_val & 0xFFFF),
                .offset_middle = @intCast(u16, (interrupt_vector_val >> 16) & 0xFFFF),
                .offset_high = @intCast(u32, (interrupt_vector_val >> 32) & 0xFFFF_FFFF),
                .selector = selector,
                .ist_index = ist,
                .attributes = attributes,
            };
        }
    };
    pub const Idt = []IdtEntry;

    pub const Idtr = packed struct {
        limit: u16,
        offset: [*]IdtEntry,
        pub fn new(_idt: Idt) @This() {
            return @This(){
                .limit = @intCast(u16, _idt.len * @sizeOf(IdtEntry) - 1),
                .offset = _idt.ptr,
            };
        }
    };

    pub extern fn asm_lidt(idtr: ?*Idtr) callconv(.C) void;
};

pub const Selector = packed struct {
    ring: u2,
    ldt: bool,
    index: u13,
    pub fn castTo(self: Selector, comptime T: type) T {
        return @intCast(T, @bitCast(u16, self));
    }

    pub fn load_cs(self: Selector) void {
        var full_selector: u64 = 0;
        full_selector = self.castTo(u64);
        asm_load_cs(full_selector);
    }
    extern fn asm_load_cs(u64) callconv(.C) void;

    pub fn load_ds(self: Selector) void {
        var full_selector = @bitCast(u16, self);
        asm_load_ds(full_selector);
    }
    extern fn asm_load_ds(u16) callconv(.C) void;

    pub fn load_es(self: Selector) void {
        asm_load_es(@bitCast(u16, self));
    }
    extern fn asm_load_es(u16) callconv(.C) void;

    pub fn load_fs(self: Selector) void {
        asm_load_fs(@bitCast(u16, self));
    }
    extern fn asm_load_fs(u16) callconv(.C) void;

    pub fn load_gs(self: Selector) void {
        @extern(*const fn (u16) callconv(.C) void, .{ .name = "asm_load_gs", .linkage = .Strong })(@bitCast(u16, self));
    }
    extern fn asm_load_gs(u16) callconv(.C) void;

    pub fn load_ss(self: Selector) void {
        asm_load_ss(@bitCast(u16, self));
    }
    extern fn asm_load_ss(u16) callconv(.C) void;
};

//pub const gdt = struct {
//    pub const GdtEntry = packed struct {
//        limit_low: u16,
//        base_low: u16,
//        base_middle: u8,
//        type_flags: Type,
//        not_system: bool,
//        privelege: u2,
//        present: bool,
//        limit_high: u3,
//        limit_flags: LimitFlags,
//        base_high: u8,
//    };
//    pub const LimitFlags = packed struct {
//        available: bool,
//        long: bool,
//        size: bool,
//        granularity: bool,
//    };
//    pub const Type = packed union { code: packed struct {
//        accessed: bool,
//        read: bool,
//        conformant: bool,
//        code: bool = true,
//    }, data: packed struct {
//        accessed: bool,
//        write: bool,
//        expand_down: bool,
//        data: bool = false,
//    } };
//    pub const NullEntry = GdtEntry{
//        .limit_low = 0,
//        .base_low = 0,
//        .base_middle = 0,
//        .type_flags = .{ .data = .{
//            .accessed = false,
//            .write = false,
//            .expand_down = false,
//        } },
//        .not_system = false,
//        .privelege = 0,
//        .present = false,
//        .limit_high = 0,
//        .limit_flags = .{
//            .available = false,
//            .long = false,
//            .size = false,
//            .granularity = false,
//        },
//        .base_high = 0,
//    };
//    pub const Gdt = []GdtEntry;
//    pub const Gdtr = packed struct {
//        limit: u16,
//        offset: *const Gdt,
//        pub fn new(_gdt: Gdt) @This() {
//            return @This(){
//                .limit = @intCast(u16, _gdt.len),
//                .offset = &_gdt,
//            };
//        }
//    };
//    pub extern fn asm_lgdt(gdtr: *Gdtr) callconv(.C) void;
//};

pub const gdt = struct {
    pub const GdtEntry = packed struct {
        limit_low: u16,
        base_low: u16,
        base_middle: u8,
        access: u8,
        limit_high: u8,
        base_high: u8,
        pub fn new(limit: u32, base: u32, access_flags: u8, limit_flags: u8) @This() {
            return @This(){
                .limit_low = @intCast(u16, limit & 0xFFFF),
                .limit_high = @intCast(u8, (limit & 0xF_0000) >> 16) | limit_flags,

                .base_low = @intCast(u16, base & 0xFFFF),
                .base_middle = @intCast(u8, (base & 0xFF_0000) >> 16),
                .base_high = @intCast(u8, (base & 0xFF00_0000) >> 24),

                .access = access_flags,
            };
        }
        pub fn newFlat(access_flags: u8, limit_flags: u8) @This() {
            return @This().new(0xFFFFF, 0, access_flags, limit_flags);
        }
    };
    pub const NullEntry = GdtEntry{
        .limit_low = 0,
        .base_low = 0,
        .base_middle = 0,
        .access = 0,
        .limit_high = 0,
        .base_high = 0,
    };
    pub const Gdt = []GdtEntry;
    pub const Gdtr = packed struct {
        limit: u16,
        offset: [*]GdtEntry,
        pub fn new(_gdt: Gdt) @This() {
            return @This(){
                .limit = @intCast(u16, _gdt.len * @sizeOf(GdtEntry) - 1),
                .offset = _gdt.ptr,
            };
        }
    };

    pub extern fn asm_lgdt(gdtr: *Gdtr) callconv(.C) void;
    pub const flags = struct {
        pub const access = struct {
            pub const present = (1 << 7);
            pub const privlege_kernel = (0 << 5);
            pub const privlege_user = (3 << 5);
            pub const code_or_data = (1 << 4);
            pub const executable = (1 << 3);
            pub const conformant = (1 << 2);
            pub const extend_down = (1 << 2);
            pub const readwrite = (1 << 1);
            pub const accessed = (1 << 0);
            pub const basic = present | readwrite | code_or_data;
            pub const basic_kernel = basic | privlege_kernel;
            pub const basic_user = basic | privlege_user;
        };
        pub const limit = struct {
            pub const granularity = (1 << 7);
            pub const size = (1 << 6);
            pub const long = (1 << 5);
            pub const free_bit = (1 << 4);
            pub const basic = granularity | long;
        };
    };
};

pub const CpuidReturn = packed struct {
    eax: u32,
    ebx: u32,
    ecx: u32,
    edx: u32,
    pub inline fn Cpuid(leaf: u32, input: ?u32) @This() {
        var eax: u32 = undefined;
        var ebx: u32 = undefined;
        var ecx: u32 = undefined;
        var edx: u32 = undefined;
        if (input) |in| {
            asm volatile ("cpuid"
                : [eax] "=eax" (eax),
                  [ebx] "=ebx" (ebx),
                  [ecx] "=ecx" (ecx),
                  [edx] "=edx" (edx),
                : [leaf] "eax" (leaf),
                  [in] "ecx" (in),
            );
        } else {
            asm volatile ("cpuid"
                : [eax] "=eax" (eax),
                  [ebx] "=ebx" (ebx),
                  [ecx] "=ecx" (ecx),
                  [edx] "=edx" (edx),
                : [leaf] "eax" (leaf),
            );
        }

        return @This(){
            .eax = eax,
            .ebx = ebx,
            .ecx = ecx,
            .edx = edx,
        };
    }
};
