const std = @import("std");
const stdext = @import("stdext");
pub const page_size = 4096;

pub const VirtAddr = packed struct {
    addr: u64,
    pub fn init(value: u64) error{InvalidAddress}!VirtAddr {
        return switch (stdext.bitops.getBits(value, 47, 64)) {
            0, 0x1FFFF => VirtAddr{ .addr = value },
            1 => initWithExtension(value),
            else => error.InvalidAddress,
        };
    }

    pub fn initWithExtension(value: u64) VirtAddr {
        return VirtAddr{ .addr = stdext.bitops.signExtend(value, 47) };
    }

    pub fn initUnchecked(value: u64) VirtAddr {
        return VirtAddr{ .addr = value };
    }

    pub fn isNull(self: VirtAddr) bool {
        return self.addr == 0;
    }

    pub fn toPhys(self: VirtAddr, phys_offset: u64) ?PhysAddr {
        var table = getLevel4PageTablePointer(phys_offset);
        var level = PageTableLevel.four;
        while (level.getLowerLevel()) |next_level| {
            level = next_level;
            var entry = table.getEntryAtIndex(PageTableIndex.fromVirtAddr(self, level));
            if (entry.isUnused()) {
                return null;
            }
            var addr = entry.getPhysAddr().toVirtAddr(phys_offset) catch {
                return null;
            };
            table = addr.toPtr(*PageTable);
        }
        // This should be the last level table now, so we'll just get the entry there, and go from there
        var entry = table.getEntryAtIndex(PageTableIndex.fromVirtAddr(self, level));
        return entry.getPhysAddr();
    }

    pub fn fromPtr(ptr: anytype) error{InvalidAddress}!VirtAddr {
        comptime if (@typeInfo(@TypeOf(ptr)) != .Pointer) @compileError("ptr is not a pointer");
        return init(@ptrToInt(ptr));
    }

    pub fn fromPtrUnchecked(ptr: anytype) error{InvalidAddress}!VirtAddr {
        comptime if (@typeInfo(@TypeOf(ptr)) != .Pointer) @compileError("ptr is not a pointer");
        return initUnchecked(@ptrToInt(ptr));
    }

    pub fn toPtr(self: VirtAddr, comptime T: type) T {
        return @intToPtr(T, self.addr);
    }

    pub fn alignUp(self: VirtAddr, align_factor: usize) VirtAddr {
        return initUnchecked(std.mem.alignForward(self.addr, align_factor));
    }

    pub fn alignDown(self: VirtAddr, align_factor: usize) VirtAddr {
        return initUnchecked(std.mem.alignBackward(self.addr, align_factor));
    }

    pub fn isAligned(self: VirtAddr, align_factor: usize) bool {
        return std.mem.isAligned(self.addr, align_factor);
    }

    pub fn add(self: VirtAddr, offset: usize) VirtAddr {
        return initUnchecked(self.addr + offset);
    }
    pub fn sub(self: VirtAddr, offset: usize) VirtAddr {
        return initUnchecked(self.addr - offset);
    }

    pub fn format(value: VirtAddr, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;
        try writer.print("VirtAddr(0x{X})", .{value.addr});
    }
};

pub const PhysAddr = packed struct {
    addr: u64,

    const MASK: u64 = std.math.maxInt(u12) << 52;
    pub fn init(value: u64) error{InvalidAddress}!PhysAddr {
        return switch (stdext.bitops.getBits(value, 52, 64)) {
            0 => PhysAddr{ .addr = value },
            else => error.InvalidAddress,
        };
    }

    pub fn initUnchecked(value: u64) PhysAddr {
        return PhysAddr{ .addr = value };
    }

    pub fn initFix(value: u64) PhysAddr {
        return PhysAddr{ .addr = value & ~MASK };
    }

    pub fn fromPtr(ptr: anytype) error{InvalidAddress}!PhysAddr {
        comptime if (@typeInfo(@TypeOf(ptr)) != .Pointer) @compileError("ptr is not a pointer");
        return init(@ptrToInt(ptr));
    }

    pub fn fromPtrUnchecked(ptr: anytype) error{InvalidAddress}!PhysAddr {
        comptime if (@typeInfo(@TypeOf(ptr)) != .Pointer) @compileError("ptr is not a pointer");
        return initUnchecked(@ptrToInt(ptr));
    }

    pub fn toPtr(self: PhysAddr, comptime T: type) T {
        return @intToPtr(T, self.addr);
    }

    pub fn isNull(self: PhysAddr) bool {
        return self.addr == 0;
    }

    pub fn alignUp(self: PhysAddr, align_factor: usize) PhysAddr {
        return initFix(std.mem.alignForward(self.addr, align_factor));
    }

    pub fn alignDown(self: PhysAddr, align_factor: usize) PhysAddr {
        return initFix(std.mem.alignBackward(self.addr, align_factor));
    }

    pub fn isAligned(self: PhysAddr, align_factor: usize) bool {
        return std.mem.isAligned(self.addr, align_factor);
    }

    pub fn toVirtAddr(self: PhysAddr, virtual_offset: u64) error{InvalidAddress}!VirtAddr {
        return VirtAddr.init(self.addr + virtual_offset);
    }

    pub fn add(self: PhysAddr, offset: usize) PhysAddr {
        return initFix(self.addr + offset);
    }
    pub fn sub(self: PhysAddr, offset: usize) PhysAddr {
        return initFix(self.addr - offset);
    }

    pub fn format(value: PhysAddr, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;

        try writer.print("PhysAddr(0x{X})", .{value.addr});
    }
};

pub const PageTableEntry = packed struct {
    entry: u64,
    pub fn init() @This() {
        return .{ .entry = 0 };
    }

    pub fn getFlags(self: PageTableEntry) PageTableEntryFlags {
        return PageTableEntryFlags.fromU64(self.entry);
    }

    pub fn getPhysAddr(self: PageTableEntry) PhysAddr {
        return PhysAddr.initUnchecked(self.entry & 0x000F_FFFF_FFFF_F000);
    }

    pub fn setPhysAddr(self: *PageTableEntry, addr: PhysAddr) void {
        std.debug.assert(addr.isAligned(page_size));
        self.entry = addr.addr | self.getFlags().toU64();
    }

    pub fn setUnused(self: *PageTableEntry) void {
        self.entry = 0;
    }

    pub fn setFlags(self: *PageTableEntry, flags: PageTableEntryFlags) void {
        self.entry = self.getPhysAddr().addr | flags.toU64();
    }

    pub fn isUnused(self: PageTableEntry) bool {
        return self.entry == 0;
    }

    pub fn format(value: PageTableEntry, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;

        if (value.isUnused()) {
            try writer.print("PageTableEntry(unused)", .{});
        } else {
            try writer.print("PageTableEntry({}, flags = {})", .{ value.getPhysAddr(), value.getFlags() });
        }
    }
};

pub const PageTableEntryFlags = packed struct {
    present: bool = false,
    writable: bool = false,
    user_accessable: bool = false,
    write_through: bool = false,
    no_cache: bool = false,
    accessed: bool = false,
    dirty: bool = false,
    huge: bool = false,
    global: bool = false,

    /// Can be used to store custom flags
    available1: u3 = 0,

    reserved1: u4 = 0,
    reserved2: u32 = 0,
    reserved3: u4 = 0,

    /// Can be used to store custom flags
    available2: u11 = 0,
    no_execute: bool = false,

    pub const RESERVED: u64 = reserved_flags: {
        var flags = std.mem.zeroes(@This());
        flags.reserved1 = std.math.maxInt(u4);
        flags.reserved2 = std.math.maxInt(u32);
        flags.reserved3 = std.math.maxInt(u4);
        break :reserved_flags @bitCast(u64, flags);
    };

    pub const NOT_RESERVED: u64 = ~RESERVED;

    pub fn toU64(self: @This()) u64 {
        return @bitCast(u64, self) & NOT_RESERVED;
    }
    pub fn fromU64(value: u64) @This() {
        return @bitCast(@This(), value & NOT_RESERVED);
    }

    pub fn makeParentFlags(self: @This()) @This() {
        var flags = @This(){};
        if (self.present)
            flags.present = true;
        if (self.writable)
            flags.writable = true;
        if (self.user_accessable)
            flags.user_accessable = true;
        return flags;
    }

    pub fn andFlags(self: @This(), other: @This()) @This() {
        return @bitCast(@This(), (@bitCast(u64, self) & @bitCast(u64, other)) & NOT_RESERVED);
    }

    pub fn orFlags(self: @This(), other: @This()) @This() {
        return @bitCast(@This(), (@bitCast(u64, self) | @bitCast(u64, other)) & NOT_RESERVED);
    }

    pub fn format(value: @This(), comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        return stdext.format.formatFlagStruct(value, fmt, options, writer);
    }
};

pub const PageTableLevel = enum(u6) {
    one = 1,
    two,
    three,
    four,
    pub fn getLowerLevel(self: PageTableLevel) ?PageTableLevel {
        if (self == .one) {
            return null;
        }
        return @intToEnum(PageTableLevel, @enumToInt(self) - 1);
    }
};

pub const PageTableIndex = struct {
    value: u9,

    pub fn init(index: u9) PageTableIndex {
        return PageTableIndex{ .value = index };
    }

    pub fn fromVirtAddr(addr: VirtAddr, level: PageTableLevel) PageTableIndex {
        return PageTableIndex{ .value = (@truncate(u9, addr.addr >> 12 >> ((@enumToInt(level) - 1) * 9))) };
    }

    pub fn toVirtAddr(self: PageTableIndex, level: PageTableLevel) VirtAddr {
        return VirtAddr.initWithExtension(@intCast(u64, self.value << 12 << ((@enumToInt(level) - 1) * 9)));
    }
    pub fn format(index: PageTableIndex, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;

        try writer.print("PageTableIndex({})", .{index.value});
    }
};

pub const PageTable = extern struct {
    entries: [512]PageTableEntry = [_]PageTableEntry{PageTableEntry.init()} ** 512,

    pub fn zero(self: *PageTable) void {
        for (self.entries) |*entry| {
            entry.entry = 0;
        }
    }

    pub fn getEntryAtIndex(self: *PageTable, index: PageTableIndex) *PageTableEntry {
        return &self.entries[index.value];
    }
    pub fn format(value: PageTable, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;
        for (value.entries) |entry, i| {
            try writer.print("Entry {}: {}\n", .{ i, entry });
        }
    }
};

extern fn getcr3() callconv(.C) u64;
extern fn setcr3(addr: u64) callconv(.C) void;
extern fn asm_invlpg(addr: u64) callconv(.C) void;

pub fn getCR3() PhysAddr {
    return PhysAddr.initUnchecked(getcr3());
}

pub fn setCR3(address: PhysAddr) void {
    setcr3(address.addr);
}

pub fn getLevel4PageTablePointer(physical_offset: u64) *PageTable {
    var virtual_cr3 = PhysAddr.initFix(getcr3()).toVirtAddr(physical_offset) catch {
        unreachable;
    };
    return virtual_cr3.toPtr(*PageTable);
}

pub fn reloadCR3() void {
    setCR3(getCR3());
}
pub fn invlpg(virtual_address: VirtAddr) void {
    asm_invlpg(virtual_address.addr);
}
