pub const std = @import("std");
pub const cpu = @import("cpu.zig");

pub const idt_size = 256;

pub var os_interrupt_handler: *const fn (interrupt_frame: *cpu.interrupts.InterruptFrame) void = defaultInterruptHandler;

pub export var static_idt: [idt_size]cpu.idt.IdtEntry = undefined;
pub export fn interruptHandler(interrupt_frame: *cpu.interrupts.InterruptFrame) callconv(.C) void {
    os_interrupt_handler(interrupt_frame);
}

pub fn defaultInterruptHandler(interrupt_frame: *cpu.interrupts.InterruptFrame) void {
    std.debug.panic("Interrupt no. {}: {s}\nInterrupt frame: {}", .{ @enumToInt(interrupt_frame.int_no), interrupt_frame.int_no.getFaultName(), interrupt_frame });
}
comptime {
    @export(interruptHandler, .{ .name = "_x86_64_isr_handler", .linkage = .Strong });
}
