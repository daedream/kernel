const prelude = @import("prelude.zig");
const arch = prelude.arch;
const bootloader = prelude.bootloader;
const log = prelude.log;
const std = @import("std");

var already_panicking: bool = false;
pub fn panic(msg: []const u8, stack_trace: ?*std.builtin.StackTrace, ret_addr: ?usize) noreturn {
    @setCold(true);
    if (already_panicking) {
        log.print("\nDOUBLE PANIC!!!\n", .{});
        arch.halt();
    }
    already_panicking = true;
    log.print("\n========\nPANIC!!!\nPanic message: {s}\n", .{msg});
    if (stack_trace) |t| {
        log.print("Stack trace: {}\n", .{t});
    }
    log.print("Return Address: {?}\n", .{ret_addr});
    log.print("Halting...\n", .{});
    log.print("========\n", .{});
    arch.halt();
}

pub fn panicInit(kargs: bootloader.KernelArgs) !void {
    _ = kargs;
}
