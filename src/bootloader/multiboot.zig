pub const arch = @import("arch");
pub const bootloader = @import("../prelude.zig").bootloader;
pub fn start() callconv(.C) noreturn {
    bootloader.load(bootloader.KernelArgs{
        .bootloader_structure = null,
    });
}
