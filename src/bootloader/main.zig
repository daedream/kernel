pub const limine = @import("limine.zig");
pub const std = @import("std");
pub const stivale = @import("stivale.zig");
pub const multiboot = @import("multiboot.zig");
pub const arch = @import("../prelude.zig").arch;
const kmain = @import("../prelude.zig").main.kmain;

/// The arguments that are passed to kmain
pub const KernelArgs = struct {
    bootloader_structure: ?*anyopaque,
    kernel_elf: File = undefined,
    physical_offset: usize = undefined,
};

comptime {
    @export(multiboot.start, .{ .name = "_start", .linkage = .Strong });
    _ = @import("limine.zig");
}

/// A general file used by the bootloader to point to a file
/// in memory
pub const File = struct {
    /// The actual bytes of the file
    address: ?*[*]u8,
    /// The size of the file
    size: u64,
    /// The read error of the file
    pub const FileReadError = error{
        SeekOutOfRange,
    };
    const FileReadContext = struct {
        f: File,
        pos: u64 = 0,
        pub fn read(self: @This(), buffer: []u8) FileReadError!usize {
            if (self.pos == self.f.size) return 0;
            var i: usize = 0;
            while (self.pos < buffer.len and self.pos < self.f.size) {
                buffer[self.pos] = self.f.address.?[self.pos];
                i += 1;
                self.pos += 1;
            }
            return i;
        }
        pub fn seekTo(self: @This(), pos: u64) FileReadError!void {
            if (pos >= self.f.size) {
                return FileReadError.SeekOutOfRange;
            }
            self.pos = pos;
        }
        pub fn seekBy(self: @This(), amt: u64) FileReadError!void {
            if (self.pos + amt >= self.f.size) {
                return FileReadError.SeekOutOfRange;
            }
            self.pos += amt;
        }
        pub fn getPos(self: @This()) !u64 {
            return self.pos;
        }
        pub fn getEndPos(self: @This()) !u64 {
            return self.f.size;
        }
    };
    /// The reader associated with a file
    pub const FileReader = std.io.Reader(FileReadContext, FileReadError, FileReadContext.read);
    /// A seekable stream for the file
    pub const FileSeekableStream = std.io.SeakableStream(FileReadContext, FileReadError, error{}, FileReadContext.seekTo, FileReadContext.seekBy, FileReadContext.getPos, FileReadContext.getEndPos);
};

/// Loads the kernel and does any setup that needs to happen before the kernel executes
pub fn load(kargs: KernelArgs) noreturn {
    // This will set up our kmain. kmain will be an extern function, so we'll import it here
    kmain(kargs);
    @import("arch").halt();
}

/// General memory map information
pub const memory_map = struct {
    pub const MemoryZoneType = enum {
        /// Memory that is readily usable
        Usable,
        /// Memory that is reserved and should
        /// not be used
        Reserved,
        /// Memory that is used by the ACPI information,
        /// but can be reclaimed by the kernel.
        AcpiReclaimable,
        /// Memory that is used for the ACPI NVS.
        AcpiNvs,
        /// Bad RAM
        BadRam,
        /// Memory that is used by the bootloader, but
        /// can be reclaimed by the kernel
        BootloaderReclaimable,
        /// Memory that is used to store the kernel (binary)
        /// and modules.
        KernelAndModules,
        /// Memory that is used for a framebuffer
        Framebuffer,
        /// Memory that could not be parsed. Should not be used.
        /// If this shows up, the kernel should panic.
        Unknown,
    };
    pub const MemmapEntry = struct {
        /// The address of the start of the entry
        address: arch.memory.PhysAddr,
        /// The length of the memory region
        length: u64,
        /// The type of memory that is described here
        type: MemoryZoneType,
    };
    /// Gets the entry marked by the index given
    pub fn getMemmapEntry(comptime bootloader_type: anytype, index: u64) !?MemmapEntry {
        switch (bootloader_type) {
            .limine => return limineGetMemmapEntry(index),
            else => {
                var iterator = try MemoryMapIterator.new();
                var i: u64 = 0;
                while (i < index) : (i += 1) {
                    _ = iterator.next();
                }
                return iterator.next();
            },
        }
    }

    fn limineGetMemmapEntry(index: u64) !?MemmapEntry {
        if (limine.memmap_request.response) |response| {
            if (index >= response.entry_count)
                return null;
            return response.entries[index].convertToStandard();
        } else {
            return error.MemoryMapResponseNotGiven;
        }
    }
    /// An iterator on the memory map
    ///
    /// This iterator has NO guarantees about order or anything of the
    /// sort.
    pub fn MemoryMapIterator(comptime bootloader_type: anytype) type {
        switch (bootloader_type) {
            .stivale => {
                return struct {
                    current_entry: u64 = 0,
                    memory_map_tag: ?*stivale.Stivale2Struct.MemoryMapTag = null,
                    pub fn init(self: *@This()) !void {
                        _ = self;
                        return error.NotImplemented;
                    }
                    pub fn next(self: *@This()) !?MemmapEntry {
                        _ = self;
                        return error.NotImplemented;
                    }
                };
            },
            .limine => {
                return struct {
                    current_entry: u64 = 0,
                    memory_map_response: *limine.MemmapRequest.Response = undefined,
                    /// Creates a new iterator
                    pub fn new() !@This() {
                        if (limine.memmap_request.response == null) {
                            return error.MemoryMapResponseNotGiven;
                        }
                        return @This(){ .memory_map_response = limine.memmap_request.response.? };
                    }
                    /// Gets the next entry in the iterator.
                    pub fn next(self: *@This()) ?MemmapEntry {
                        if (self.current_entry >= self.memory_map_response.entry_count) {
                            return null;
                        }
                        var current_entry = self.memory_map_response.entries[self.current_entry].*;
                        self.current_entry += 1;
                        return current_entry.convertToStandard();
                    }
                };
            },
        }
    }
};
