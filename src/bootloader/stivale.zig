const boot = @import("main.zig");
const std = @import("std");

pub const Stivale2Anchor = extern struct {
    anchor: [15]u8,
    bits: u8,
    phys_load_addr: u64,
    phys_bss_start: u64,
    phys_bss_end: u64,
    phys_stivale2hdr: u64,
};

pub const Stivale2Header = extern struct {
    entry_point: ?fn (stivale2_struct: *Stivale2Struct) callconv(.C) noreturn = null,
    stack: *align(16) const u8,
    flags: Flags,
    tags: ?*const tags.Tag = null,

    pub const Flags = packed struct {
        /// This is reserved and unused. Must be 0
        reserved: u1 = 0,
        /// Set to 1 for a higher half kernel
        higher_half: u1 = 0,
        /// Set for protected memory ranges (PMRs)
        /// This will map the ELF sections with the
        /// permissions that the sections would normally
        /// have
        pmr: u1 = 0,
        /// Set for fully virtual mapping for PMRs.
        /// This requires `pmr` to be set.
        fully_virtual_mapping: u1 = 0,
        /// This should ALWAYS be set as it is
        /// a depreciated function
        boot_if_lma_fail: u1 = 1,
        /// Padding
        zeroes: u59 = 0,
    };

    pub const tags = struct {
        pub const Identifier = enum(u64) {
            any_video = 0xc75c9fa92a44c4db,
            framebuffer = 0x3ecc1bc43d0f7971,
        };
        pub const Tag = packed struct {
            identifier: Identifier,
            next: ?*const Tag = null,
        };

        pub const AnyVideoTag = packed struct {
            tag: Tag = .{ .identifier = Identifier.any_video },
            preference: FramebufferPreference,
            pub const FramebufferPreference = enum(u64) {
                linear_framebuffer = 0,
                no_linear_framebuffer = 1,
            };
        };

        pub const FramebufferTag = packed struct {
            tag: Tag = .{ .identifier = Identifier.framebuffer },
            width_hint: u16 = 0,
            height_hint: u16 = 0,
            bpp_hint: u16 = 0,
            unused: u16 = 0,
        };
    };
};

pub const Stivale2Struct = extern struct {
    pub const stivale2_bootloader_brand_size = 64;
    pub const stivale2_bootloader_version_size = 64;
    bootloader_brand: [stivale2_bootloader_brand_size:0]u8,
    bootloader_version: [stivale2_bootloader_version_size:0]u8,
    tags: u64,
    pub const TagHeader = extern struct {
        identifier: u64,
        next: u64,
    };
    pub const TagIterator = struct {
        current: ?*TagHeader,
        pub fn next(self: *TagIterator) ?*TagHeader {
            if (self.current == null)
                return null;
            self.current = @intToPtr(?*TagHeader, @intCast(usize, self.current.?.next));
            return self.current;
        }
        pub fn new(s: Stivale2Struct) TagIterator {
            return @This(){ .current = @intToPtr(?*TagHeader, @intCast(usize, s.tags)) };
        }
    };
    pub const PmrTag = extern struct {
        header: TagHeader,
        entries: u64,
        pmrs: [*]Pmr,
        pub const identifier = 0x5df266a64047b6bd;
        pub const Pmr = extern struct {
            base: u64,
            length: u64,
            permissions: u64,
            const executable = 1 << 0;
            const writable = 1 << 1;
            const readable = 1 << 2;
        };
    };
    pub const KernelFileV2Tag = extern struct {
        header: TagHeader,
        file: u64,
        size: u64,
        pub const identifier = 0x37c13018a02c6ea2;
    };
    pub const MemoryMapTag = extern struct {
        header: TagHeader,
        entries: u64,
        memmap: [*]MemoryMapEntry,
    };
    pub fn findTag(self: Stivale2Struct, comptime tagType: type) ?*tagType {
        var tag_iterator = Stivale2Struct.TagIterator.new(self);
        while (tag_iterator.next()) |tag| {
            if (tag.identifier == tagType.IDENTIFIER) {
                return @ptrCast(*tagType, tag);
            }
        }
        return null;
    }
};

pub const MemoryMapEntry = extern struct {
    base: u64,
    length: u64,
    type: MemoryMapEntryType,
    unused: u32,
    pub const MemoryMapEntryType = enum(u32) {
        Usable = 0,
        Reserved = 1,
        AcpiReclaimable = 2,
        AcpiNvs = 3,
        BadMemory = 4,
        BootloaderReclaimable = 0x1000,
        KernelAndModules = 0x1001,
        Framebuffer = 0x1002,
    };
};

const framebuffer_tag = Stivale2Header.tags.FramebufferTag{};
pub export var stack_bytes: [8192:0]u8 align(16) linksection(".bss") = undefined;
pub var stivale2_header align(4) = Stivale2Header{
    .stack = &stack_bytes[8192],
    .flags = Stivale2Header.Flags{ .higher_half = 1, .pmr = 1, .fully_virtual_mapping = 1 },
    .tags = &framebuffer_tag.tag,
    .entry_point = start,
};
comptime {
    @export(stivale2_header, .{ .name = "stivale_hdr", .section = ".stivale2hdr", .linkage = .Strong });
}

pub fn start(stivale2_struct: *Stivale2Struct) callconv(.C) noreturn {
    boot.load(boot.KernelArgs{
        .kernel_elf = .{ .address = null, .size = 0 },
        .bootloader_structure = stivale2_struct,
    });
}
