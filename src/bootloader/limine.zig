const bootloader = @import("main.zig");
const std = @import("std");
const arch = @import("../prelude.zig").arch;

const limine_magic1 = 0xc7b1dd30df4c8b88;
const limine_magic2 = 0x0a82e883a194f07b;
/// Can be used to create a request for the bootloader
pub fn Request(comptime RequestType: type) type {
    return extern struct {
        /// The response type of the request
        pub const Response = RequestType.Response;
        /// The ID of the response. Set by the request
        id: [4]u64 = .{ limine_magic1, limine_magic2, RequestType.magic1, RequestType.magic2 },
        /// The revision number of the request given
        revision: u64 = RequestType.revision,
        /// A pointer to the response requested.
        /// This will stay null if the bootloader could not handle it
        response: ?*RequestType.Response = null,
        /// The actual request itself.
        request: RequestType = undefined,
    };
}

pub const EntryPointRequest = Request(extern struct {
    const magic1 = 0x13d86c035a1cd3e1;
    const magic2 = 0x2b0caa89d8f3026a;
    const revision = 0;
    entry_point: *const fn () callconv(.C) noreturn = undefined,
    pub const Response = extern struct {
        revision: u64 = undefined,
    };
});

pub const BootloaderInfoRequest = Request(extern struct {
    const magic1 = 0xf55038d8e2a1202f;
    const magic2 = 0x279426fcf5f59740;
    const revision = 0;
    pub const Response = extern struct {
        revision: u64 = undefined,
        name: [*:0]u8 = undefined,
        version: [*:0]u8 = undefined,
    };
});

pub const StackSizeRequest = Request(extern struct {
    const magic1 = 0x224ef0460a8e8926;
    const magic2 = 0xe1cb0fc25f46ea3d;
    const revision = 0;
    /// The size of the stack requested
    stack_size: u64,
    pub const Response = extern struct {
        revision: u64 = undefined,
    };
});

pub const HddmRequest = Request(extern struct {
    const magic1 = 0x48dcf1cb8ad2b852;
    const magic2 = 0x63984e959a98244b;
    const revision = 0;
    pub const Response = extern struct {
        revision: u64 = undefined,
        offset: u64 = undefined,
    };
});

pub const TerminalRequest = Request(extern struct {
    const magic1 = 0x0785a0aea5d0750f;
    const magic2 = 0x1c1936fee0d6cf6e;
    const revision = 0;
    callback: TerminalCallback,

    pub const TerminalCallback = fn (*Terminal, u64, u64, u64, u64) callconv(.C) void;
    pub const Response = extern struct {
        revision: u64,
        terminal_count: u64,
        terminals: [*]*Terminal,
        terminal_write: TerminalWriter,
    };
    pub const TerminalWriter = fn (*Terminal, [*:0]u8, u64) callconv(.C) void;
});

pub const Terminal = extern struct {
    columns: u32,
    rows: u32,
    framebuffer: *Framebuffer,
};

pub const Framebuffer = extern struct {
    address: ?*anyopaque,
    width: u16,
    height: u16,
    pitch: u16,
    bpp: u16,
    memory_model: u8,
    red_mask_size: u8,
    red_mask_shift: u8,
    green_mask_size: u8,
    green_mask_shift: u8,
    blue_mask_size: u8,
    blue_mask_shift: u8,
    unused: u8,
    edid_size: u64,
    edid: ?*anyopaque,
};

pub const FiveLevelPagingRequest = Request(extern struct {
    const magic1 = 0x94469551da9b3192;
    const magic2 = 0xebe5e86db7382888;
    const revision = 0;
    pub const Response = extern struct {
        revision: u64,
    };
});

pub const SmpInfo = extern struct {
    processor_id: u32,
    lapic_id: u32,
    reserved: u64,
    goto_address: ?LimineGotoAddress,
    extra_argument: u64,
    pub const LimineGotoAddress = fn (*SmpInfo) callconv(.C) void;
};
pub const SmpRequest = Request(extern struct {
    const magic1 = 0x95a67b819a1b857e;
    const magic2 = 0xa0b61b723b6a73e0;
    const revision = 0;
    pub const Response = extern struct {
        revision: u64,
        flags: u32,
        bsp_lapic_id: u32,
        cpu_count: u64,
        cpus: [*]SmpInfo,
    };
});

pub const MemmapRequest = Request(extern struct {
    const magic1 = 0x67cf3d9d378a806f;
    const magic2 = 0xe304acdfc50c3c62;
    const revision = 0;
    /// The response requested
    pub const Response = extern struct {
        /// The bootloader's revision of the response
        revision: u64,
        /// The number of entries are given
        entry_count: u64,
        /// An array of pointers to memory map entries
        entries: [*]*MemmapEntry,
    };
});

/// Entries to the memory map
pub const MemmapEntry = extern struct {
    base: u64,
    length: u64,
    type: Type,
    pub const Type = enum(u64) {
        Usable = 0,
        Reserved = 1,
        AcpiReclaimable = 2,
        AcpiNvs = 3,
        BadMemory = 4,
        BootloaderReclaimable = 5,
        KernelAndModules = 6,
        Framebuffer = 7,
        _,
    };
    /// Converts the limine entry to a standard memory entry, that the bootloader
    /// will give to the kernel
    pub fn convertToStandard(entry: @This()) bootloader.memory_map.MemmapEntry {
        return bootloader.memory_map.MemmapEntry{ .address = arch.memory.PhysAddr.init(entry.base) catch unreachable, .length = entry.length, .type = switch (entry.type) {
            .Usable => .Usable,
            .Reserved => .Reserved,
            .AcpiReclaimable => .AcpiReclaimable,
            .AcpiNvs => .AcpiNvs,
            .BadMemory => .BadRam,
            .BootloaderReclaimable => .BootloaderReclaimable,
            .KernelAndModules => .KernelAndModules,
            .Framebuffer => .Framebuffer,
            _ => .Unknown,
        } };
    }
};

pub const KernelFileRequest = Request(extern struct {
    const magic1 = 0xad97e90e83f1ed67;
    const magic2 = 0x31eb5d1c5ff23b69;
    const revision = 0;
    pub const Response = extern struct {
        revision: u64 = undefined,
        file: *File = undefined,
    };
});

pub const ModuleRequest = Request(extern struct {
    const magic1 = 0x3e7e279702be32af;
    const magic2 = 0xca1c4f3bd1280cee;
    const revision = 0;
    pub const Response = extern struct {
        revision: u64,
        count: u64,
        modules: [*]*File,
    };
});

pub const Uuid = extern struct {
    a: u32,
    b: u16,
    c: u16,
    d: [8]u8,
};

pub const File = extern struct {
    revision: u64,
    address: ?*[*]u8,
    size: u64,
    path: [*:0]u8,
    cmdline: [*:0]u8,
    media_type: MediaType,
    unused: u32,
    tftp_ip: u32,
    tftp_port: u32,
    partition_index: u32,
    mbr_disk_id: u32,
    gpt_disk_uuid: Uuid,
    gpt_part_uuid: Uuid,
    part_uuid: Uuid,
    pub const MediaType = enum(u32) {
        Generic = 0,
        Optical = 1,
        Tftp = 2,
    };

    /// Will convert the file given by limine to
    /// the standard format the kernel uses
    pub fn convertToStandard(self: @This()) bootloader.File {
        return .{ .address = self.address.?, .size = self.size };
    }
};

pub var bootloader_info_request = BootloaderInfoRequest{ .revision = 0 };
pub var stack_size_request = StackSizeRequest{ .revision = 0, .request = .{ .stack_size = 64 * 1024 } };
pub var memmap_request = MemmapRequest{};
pub var kernel_file_request = KernelFileRequest{};
pub var entry_point_request = EntryPointRequest{ .request = .{ .entry_point = start } };
pub var hddm_request = HddmRequest{};
pub export var limine_requests linksection(".limine_reqs") = [_:null]?*const anyopaque{
    @ptrCast(?*const anyopaque, &bootloader_info_request),
    @ptrCast(?*const anyopaque, &stack_size_request),
    @ptrCast(?*const anyopaque, &memmap_request),
    @ptrCast(?*const anyopaque, &kernel_file_request),
    @ptrCast(?*const anyopaque, &entry_point_request),
    @ptrCast(?*const anyopaque, &hddm_request),
};

pub fn start() callconv(.C) noreturn {
    bootloader.load(bootloader.KernelArgs{
        .kernel_elf = kernel_file_request.response.?.file.convertToStandard(),
        .bootloader_structure = null,
        .physical_offset = @intCast(u64, hddm_request.response.?.offset),
    });
}
