pub const build_options = @import("build_options");

pub const main = @import("main.zig");

pub const current_arch = build_options.arch;
pub const arch = switch (current_arch) {
    .x86_64 => @import("arch/x86_64/main.zig"),
};

comptime {
    _ = switch (current_arch) {
        .x86_64 => @import("arch/x86_64/main.zig"),
    };
}

pub const bootloader = @import("bootloader/main.zig");
comptime {
    _ = @import("bootloader/main.zig");
}

pub const log = @import("log.zig");

pub const mm = @import("mm.zig");
