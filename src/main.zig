const prelude = @import("prelude.zig");
const bootloader = prelude.bootloader;
const panic_stuff = @import("panic.zig");
const pmm = prelude.mm.pmm;
const interrupts = @import("interrupts.zig");
const vmm = prelude.mm.vmm;
const log = prelude.log;
const arch = prelude.arch;
const std = @import("std");

var arch_logger: log.KernelWriterBase = undefined;

/// Where kernel execution technically starts
/// In reality, it does one final bootstrapping,
/// for language ergonomics.
pub fn kmain(kargs: bootloader.KernelArgs) noreturn {
    arch_logger = arch.loggingInit() catch {
        // This is actually difficult, since we have no logging abilities yet.
        // We'll just hang, and hope it becomes obvious once we
        // debug.
        arch.halt();
    };
    log.init(arch_logger);
    defer arch.loggingClose(arch_logger) catch |err| {
        std.debug.panic("Could not close arch logger! {}", .{err});
    }; // This invalidates the logger, so be careful!

    // By this point, serial is working, which is our
    // basic logger, so we can run kmain for real
    trueKmain(kargs) catch |err| {
        std.debug.panic("kmain died with error {}", .{err});
    };
    std.debug.panic("kmain ended!", .{});
    arch.halt();
}

/// This is where the kernel actually starts. This is what sets
/// up the entirety of the kernel
pub fn trueKmain(kargs: bootloader.KernelArgs) anyerror!void {
    log.log(.Info, "kargs: {}", .{kargs});
    try panic_stuff.panicInit(kargs);
    try arch.archInit();
    try interrupts.init();
    try pmm.init();
    try vmm.init(kargs.physical_offset);
    var page = try pmm.allocator.allocHigh();
    log.log(.Debug, "allocated page {}", .{page});
    defer pmm.allocator.free(page) catch |err| {
        log.log(.Error, "Error freeing: {}\n", .{err});
    };
    var virtAddr = arch.memory.VirtAddr.initUnchecked(0xb00b135);
    log.log(.Debug, "Mapping {} to {}", .{ page, virtAddr.alignDown(4096) });
    try vmm.mapper.map(page, virtAddr.alignDown(4096), .{ .write = true });
    // Check the physical address
    log.log(.Debug, "{} is mapped to {?}", .{ virtAddr.alignDown(4096), virtAddr.toPhys(kargs.physical_offset) });
    var virtAddrPtr = virtAddr.alignDown(@sizeOf(usize)).toPtr(*usize);
    virtAddrPtr.* = 0xFF;
    log.log(.Debug, "deallocating", .{});
    try vmm.mapper.unmap(virtAddr.alignDown(4096));
    virtAddrPtr.* = 0xFF;
    log.log(.Debug, "Should not get here", .{});
}

// A stub. Just calls panic in panic.zig
pub fn panic(msg: []const u8, stack_trace: ?*std.builtin.StackTrace, ret_addr: ?usize) noreturn {
    @setCold(true);
    panic_stuff.panic(msg, stack_trace, ret_addr);
}
