const std = @import("std");
const prelude = @import("../prelude.zig");
const arch = prelude.arch;
const mm = @import("../mm.zig");

pub const PageMapper = @import("page_mapper.zig").PageMapper;

pub var mapper: PageMapper = undefined;

pub const PageMapError = error{
    PageAllocError,
};

pub fn init(phys_offset: u64) !void {
    mm.physical_offset = phys_offset;
    mapper = try arch.paging.init();
}
