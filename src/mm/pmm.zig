/// Abstraction over the page allocation
const prelude = @import("../prelude.zig");
const stdext = @import("stdext");

pub const PageAllocator = @import("page_allocator.zig").PageAllocator;
pub const max_physical_ram = 32 * (1024 * 1024 * 1024); // 32 GiB
pub const page_size = prelude.arch.memory.page_size;

pub const bitmap_allocator = @import("bitmap_allocator.zig");

pub const allocator_type = prelude.build_options.allocator_type;

pub const pmm_space_needed = switch (allocator_type) {
    .bitmap => stdext.bitmap.bitCountToBackingSize(max_physical_ram / prelude.arch.memory.page_size),
};

pub var pmm_space: [pmm_space_needed]usize = undefined;

pub var allocator: PageAllocator = undefined;

pub fn init() !void {
    switch (allocator_type) {
        .bitmap => {
            try bitmap_allocator.init(&pmm_space);
            allocator = bitmap_allocator.pageAllocator();
        },
    }
}
