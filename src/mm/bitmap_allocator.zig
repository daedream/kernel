/// This is a simple page frame allocator
const bitmap = @import("stdext").bitmap;
const prelude = @import("../prelude.zig");
const arch = prelude.arch;
const bootloader = prelude.bootloader;
const PhysAddr = arch.memory.PhysAddr;
const PageAllocator = prelude.mm.pmm.PageAllocator;
const std = @import("std");

/// An iterator over the bootloader provided memory maps
pub const MMapIterator = bootloader.memory_map.MemoryMapIterator(@import("build_options").bootloader);

const allocator_vtable = PageAllocator.VTable{
    .allocLow = allocLow,
    .allocHigh = allocHigh,
    .free = free,
    .setUsed = setUsed,
    .setUnused = setUnused,
    .getUsed = getUsed,
    .allocContiguousLow = allocContiguousLow,
    .allocContiguousHigh = allocContiguousHigh,
    .setRangeUnused = setRangeUnused,
    .setRangeUsed = setRangeUsed,
    .freeRange = freeRange,
};

var static_bitmap: bitmap.Bitmap = undefined;

pub fn init(backing_array: []usize) !void {
    prelude.log.log(.Debug, "Maximum physical memory size: {}GB", .{(backing_array.len * @sizeOf(usize) * 8 * arch.memory.page_size) / (1024 * 1024 * 1024)});
    static_bitmap = bitmap.Bitmap{
        .backing_array = backing_array,
    };
    var mmap_iterator = try MMapIterator.new();
    while (mmap_iterator.next()) |entry|
        if (entry.type == .Usable)
            try static_bitmap.unsetRangeClamped(@as(usize, entry.address.addr / arch.memory.page_size), @as(usize, entry.address.addr + entry.length) / arch.memory.page_size);
}

pub fn pageAllocator() PageAllocator {
    return PageAllocator{
        .ptr = undefined,
        .vtable = &allocator_vtable,
    };
}

fn allocLow(ctx: *anyopaque) PageAllocator.Error!PhysAddr {
    _ = ctx;
    var bitmap_free = static_bitmap.findFirstUnset() orelse return error.OutOfMemory;
    static_bitmap.set(bitmap_free) catch unreachable;
    return PhysAddr.init(bitmap_free * arch.memory.page_size) catch unreachable;
}

fn allocHigh(ctx: *anyopaque) PageAllocator.Error!PhysAddr {
    _ = ctx;
    var bitmap_free = static_bitmap.findLastUnset() orelse return error.OutOfMemory;
    static_bitmap.set(bitmap_free) catch unreachable;
    return PhysAddr.init(bitmap_free * arch.memory.page_size) catch unreachable;
}

fn free(ctx: *anyopaque, page: PhysAddr) PageAllocator.Error!void {
    _ = ctx;
    var index = page.addr / arch.memory.page_size;
    if (!(try static_bitmap.get(index))) {
        return error.DoubleFree;
    } else {
        try static_bitmap.unset(index);
    }
}

fn setUsed(ctx: *anyopaque, page: PhysAddr) void {
    _ = ctx;
    static_bitmap.set(page.addr / arch.memory.page_size) catch return;
}

fn setUnused(ctx: *anyopaque, page: PhysAddr) void {
    _ = ctx;
    static_bitmap.unset(page.addr / arch.memory.page_size) catch return;
}

fn getUsed(ctx: *anyopaque, page: PhysAddr) bool {
    _ = ctx;
    return static_bitmap.get(page.addr / arch.memory.page_size) catch return true;
}

fn allocContiguousLow(ctx: *anyopaque, size: usize) PageAllocator.Error!PhysAddr {
    _ = ctx;
    var bitmap_free = static_bitmap.findFirstRangeFree(size / arch.memory.page_size) orelse return error.OutOfMemory;
    try static_bitmap.setRange(bitmap_free, bitmap_free + size / arch.memory.page_size);
    return PhysAddr.init(bitmap_free * arch.memory.page_size) catch unreachable;
}

fn allocContiguousHigh(ctx: *anyopaque, size: usize) PageAllocator.Error!PhysAddr {
    _ = ctx;
    var bitmap_free = static_bitmap.findLastRangeFree(size / arch.memory.page_size) orelse return error.OutOfMemory;
    try static_bitmap.setRange(bitmap_free, bitmap_free + size / arch.memory.page_size);
    return PhysAddr.init(bitmap_free * arch.memory.page_size) catch unreachable;
}

fn setRangeUsed(ctx: *anyopaque, start: PhysAddr, size: usize) void {
    _ = ctx;
    static_bitmap.setRangeClamped(start.addr / arch.memory.page_size, (start.addr + size) / arch.memory.page_size) catch return;
}
fn setRangeUnused(ctx: *anyopaque, start: PhysAddr, size: usize) void {
    _ = ctx;
    static_bitmap.unsetRangeClamped(start.addr / arch.memory.page_size, (start.addr + size) / arch.memory.page_size) catch return;
}

fn freeRange(ctx: *anyopaque, start: PhysAddr, size: usize) PageAllocator.Error!void {
    _ = ctx;
    if (try static_bitmap.checkRange(start.addr / arch.memory.page_size, (start.addr + size) / arch.memory.page_size, false)) {
        return error.DoubleFree;
    }
    setRangeUnused(undefined, start, size);
}
