const std = @import("std");
const arch = @import("../prelude.zig").arch;
const PhysAddr = arch.memory.PhysAddr;
const VirtAddr = arch.memory.VirtAddr;

pub const PageMapper = struct {
    ptr: *anyopaque,
    vtable: *const VTable,
    pub const Error = error{
        OutOfMemory,
        InvalidVirtualAddress,
        InvalidPhysicalAddress,
        InvalidFlags,
    };
    pub const Flags = struct {
        // Page contains executable code
        executable: bool = false,
        // Page is use accessable (not kernel accessable)
        user: bool = false,
        // Page is writable
        write: bool = false,
        // Page is used for memory mapped io
        mmio: bool = false,
    };
    pub const VTable = struct {
        /// Map a physical address to a virtual address. Will map a page_size's worth of memory
        /// physical_address and virtual_address will be aligned downwards to a page.
        map: *const fn (ctx: *anyopaque, physical_address: PhysAddr, virtual_address: VirtAddr, flags: Flags) Error!void,
        /// Map a physical address to a virtual address with a size of size. Will align size upwards to a page_size.
        /// phsyical_address and virtual_address will be aligned downwards to a page.
        mapRange: *const fn (ctx: *anyopaque, physical_address: PhysAddr, virtual_address: VirtAddr, size: usize, flags: Flags) Error!void,
        /// Unmap a virtual address. Will unmap a page_size's worth of memory.
        /// virtual_address will be aligned downwards to a page.
        unmap: *const fn (ctx: *anyopaque, virtual_address: VirtAddr) Error!void,
        /// Unmap a virtual address. Will unmap a range of the size given.
        /// virtual_address will be aligned downwards to a page, and size will be aligned upwards to a page_size.
        unmapRange: *const fn (ctx: *anyopaque, virtual_address: VirtAddr, size: usize) Error!void,
        /// Change the flags of a virtual address given.
        /// Will align virtual_address downwards to a page.
        changeFlags: *const fn (ctx: *anyopaque, virtual_address: VirtAddr, flags: Flags) Error!void,
        /// Change the flags of a range of virtual addresses.
        /// Will align virtual_address downwards to a page, and size upwards to a page.
        changeRangeFlags: *const fn (ctx: *anyopaque, virtual_address: VirtAddr, size: usize, flags: Flags) Error!void,
    };

    pub fn map(self: @This(), physical_address: PhysAddr, virtual_address: VirtAddr, flags: Flags) Error!void {
        return self.vtable.map(self.ptr, physical_address, virtual_address, flags);
    }
    pub fn mapRange(self: @This(), physical_address: PhysAddr, virtual_address: VirtAddr, size: usize, flags: Flags) Error!void {
        return self.vtable.mapRange(self.ptr, physical_address, virtual_address, size, flags);
    }
    pub fn unmap(self: @This(), virtual_address: VirtAddr) Error!void {
        return self.vtable.unmap(self.ptr, virtual_address);
    }
    pub fn changeFlags(self: @This(), virtual_address: VirtAddr, flags: Flags) Error!void {
        return self.vtable.changeFlags(self.ptr, virtual_address, flags);
    }
};
