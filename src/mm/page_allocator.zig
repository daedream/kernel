/// Holds an abstraction to a physical page allocator
const prelude = @import("../prelude.zig");
const arch = prelude.arch;
const PhysAddr = arch.memory.PhysAddr;
const pmm = prelude.mm.pmm;
const std = @import("std");

pub const PageAllocator = struct {
    /// The pointer to the implementation
    ptr: *anyopaque,
    /// The VTable of the implementation
    vtable: *const VTable,
    /// An error returned by any allocations/freeing
    pub const Error = error{
        /// Returned when out of memory
        OutOfMemory,
        /// Returned when a double-free is done to a physical address
        DoubleFree,
        /// Returned when a given address is out of range of the physical memory allocator
        OutOfRange,
        /// Returned when a given range is invalid.
        InvalidRange,
    };
    /// A collection of functions that must be implemented by the allocator
    pub const VTable = struct {
        /// Attempt to allocate a page in the low memory area.
        /// Generally this is only used for device drivers.
        ///
        /// Returns an Error if anything goes wrong, otherwise a PhysAddr of the beginning
        /// of the page allocated.
        allocLow: *const fn (ctx: *anyopaque) Error!PhysAddr,
        /// Attempt to allocate a page in the high memory area.
        /// This is the main one that is used by everything else
        /// 
        /// Returns an Error if anything goes wrong, otherwise a PhysAddr of the beginning
        /// of the page allocated
        allocHigh: *const fn (ctx: *anyopaque) Error!PhysAddr,
        /// Frees a *singular* page given
        ///
        /// Returns an error if anything goes wrong (Including a double-free).
        free: *const fn (ctx: *anyopaque, page: PhysAddr) Error!void,
        /// Sets a page at the PhysAddr given to used. This is infalliable.
        /// If anything goes wrong, fail quietly.
        setUsed: *const fn (ctx: *anyopaque, page: PhysAddr) void,
        /// Sets a page at the PhysAddr given to unused. This is infalliable.
        /// If anything goes wrong, fail quietly.
        setUnused: *const fn (ctx: *anyopaque, page: PhysAddr) void,
        /// Returns true if a page is used. Returns used on any error
        getUsed: *const fn (ctx: *anyopaque, page: PhysAddr) bool,
        /// Attempt to allocate `len` bytes, rounded up to `arch.PAGE_SIZE`, in
        /// lower memory.
        /// Generally this is used for device drivers.
        ///
        /// Returns an Error if anything goes wrong, otherwise a PhysAddr of the beginning
        /// of the page(s) allocated
        allocContiguousLow: *const fn (ctx: *anyopaque, len: usize) Error!PhysAddr,
        /// Attempt to allocate `len` bytes, rounded up to `arch.PAGE_SIZE`, in
        /// higher memory.
        /// This is the one to use for general purpose
        ///
        /// Returns an Error if anything goes wrong, otherwise a PhysAddr of the beginning
        /// of the page(s) allocated
        allocContiguousHigh: *const fn (ctx: *anyopaque, len: usize) Error!PhysAddr,

        // For code cleanliness, put any optional functions down here

        /// Set the range, starting at start, and of length `len` bytes to
        /// used.
        ///
        /// OPTIONAL:
        /// Set to null if the default implementation is desired.
        ///
        /// This function is infalliable, and will fail quietly.
        /// THIS INCLUDES INVALID RANGES.
        setRangeUsed: ?*const fn (ctx: *anyopaque, start: PhysAddr, len: usize) void,
        /// Set the range, starting at start, and of length `len` bytes to
        /// unused.
        ///
        /// OPTIONAL:
        /// Set to null if the default implementation is desired.
        ///
        /// This function is infalliable, and will fail quietly.
        /// THIS INCLUDES INVALID RANGES.
        setRangeUnused: ?*const fn (ctx: *anyopaque, start: PhysAddr, len: usize) void,
        /// Free the range, starting at start, and of length `len` bytes, aligned upwards to
        /// `prelude.arch.memory.page_size`.
        ///
        /// OPTIONAL:
        /// Set to null if the default implementation is desired.
        freeRange: ?*const fn (ctx: *anyopaque, start: PhysAddr, len: usize) Error!void,
    };
    /// Attempt to allocate a page in the low memory area.
    /// Generally this is only used for device drivers.
    ///
    /// Returns an Error if anything goes wrong, otherwise a PhysAddr of the beginning
    /// of the page allocated.
    pub fn allocLow(self: @This()) Error!PhysAddr {
        return self.vtable.allocLow(self.ptr);
    }

    /// Attempt to allocate a page in the high memory area.
    /// This is the main one that is used by everything else
    /// 
    /// Returns an Error if anything goes wrong, otherwise a PhysAddr of the beginning
    /// of the page allocated
    pub fn allocHigh(self: @This()) Error!PhysAddr {
        return self.vtable.allocHigh(self.ptr);
    }

    /// Attempt to free a page
    /// Frees a *singular* page given
    ///
    /// Returns an error if anything goes wrong (Including a double-free).
    pub fn free(self: @This(), page: PhysAddr) Error!void {
        return self.vtable.free(self.ptr, page);
    }

    /// Sets a page at the PhysAddr given to used.
    ///
    /// This function is infalliable.
    pub fn setUsed(self: @This(), page: PhysAddr) Error!void {
        return self.vtable.setUsed(self.ptr, page);
    }

    /// Sets a page at the PhysAddr given to unused.
    ///
    /// This function is infalliable.
    pub fn setUnused(self: @This(), page: PhysAddr) Error!void {
        return self.vtable.setUnused(self.ptr, page);
    }

    /// Attempt to allocate `len` bytes, rounded up to `arch.memory.PAGE_SIZE`, in
    /// lower memory.
    /// Generally this is used for device drivers.
    ///
    /// Returns an Error if anything goes wrong, otherwise a PhysAddr of the beginning
    /// of the page(s) allocated
    pub fn allocContiguousLow(self: @This(), len: usize) Error!PhysAddr {
        return self.vtable.allocContiguousLow(self.ptr, len);
    }

    /// Attempt to allocate `len` bytes, rounded up to `arch.PAGE_SIZE`, in
    /// higher memory.
    /// This is the one to use for general purpose
    ///
    /// Returns an Error if anything goes wrong, otherwise a PhysAddr of the beginning
    /// of the page(s) allocated
    pub fn allocContiguousHigh(self: @This(), len: usize) Error!PhysAddr {
        return self.vtable.allocContiguousHigh(self.ptr, len);
    }

    /// Set the range, starting at start, and of length `len` bytes to
    /// used.
    ///
    /// This function is infalliable, and will fail quietly.
    /// THIS INCLUDES INVALID RANGES.
    pub fn setRangeUsed(self: @This(), start: PhysAddr, len: usize) void {
        if (self.vtable.setRangeUsed) |vtable| {
            return vtable(self.ptr, start, len);
        } else {
            std.debug.panic("setRangeUsed default implementation is unimplemented", .{});
        }
    }
    /// Set the range, starting at start, and of length `len` bytes to
    /// used.
    ///
    /// This function is infalliable, and will fail quietly.
    /// THIS INCLUDES INVALID RANGES.
    pub fn setRangeUnused(self: @This(), start: PhysAddr, len: usize) void {
        if (self.vtable.setRangeUnused) |vtable| {
            return vtable(self.ptr, start, len);
        } else {
            std.debug.panic("setRangeUnused default implementation is unimplemented", .{});
        }
    }

    /// Free the range, starting at start, and of length `len` bytes, aligned upwards to
    /// `prelude.arch.memory.page_size`.
    pub fn freeRange(self: @This(), start: PhysAddr, len: usize) Error!void {
        if (self.vtable.freeRange) |vtable| {
            return vtable(self.ptr, start, len);
        } else {
            std.debug.panic("freeRange default implementation is unimplemented", .{});
        }
    }

    /// Returns true if a page is used. Returns used on any error
    pub fn getUsed(self: @This(), page: PhysAddr) bool {
        return self.vtable.getUsed(self.ptr, page);
    }

    /// Dumps the current memory map to the writer passed
    /// This is a very slow function and will lock the allocator during its use, so use
    /// sparingly
    pub fn dumpMemoryMap(self: @This(), writer: anytype) !void {
        var i: usize = 0;
        var first_free: ?usize = null;
        while (i < pmm.max_physical_memory / pmm.page_size) : (i += 1) {
            if (!self.getUsed(i * pmm.page_size)) {
                if (first_free) |_| {} else {
                    first_free = i;
                }
            } else {
                if (first_free) |f| {
                    try writer.print("{x} - {x} free\n", .{ f * pmm.page_size, i * pmm.page_size });
                    first_free = null;
                }
            }
        }
        if (first_free) |f| {
            try writer.print("{x} - {x} free\n", .{ f * pmm.page_size, pmm.max_physical_memory });
        }
    }
};
