# DaeDream kernel's Makefile

# Only set the arch if it hasn't been set before
ARCH ?= x86_64

ZZ = ../../zig/build/zig2
#ZZ = zig

LD = ld.lld

# Set the assembler
# REMEMBER TO ALSO INCLUDE THE OUTPUT TARGET IN THE ASM STRING
ifeq ($(ARCH),x86_64)
	AS = nasm
	ASFLAGS = -felf64 -wall -g
endif


# Where directories end up
SRC = src
DOC = doc
ZIGOUT = zig-out
OBJDIR = $(ZIGOUT)/obj
ARCHDIR = $(SRC)/arch/$(ARCH)
ARCHASM = $(ARCHDIR)/asm

ARCHASMSOURCES = $(shell find $(ARCHASM) -name '*.asm')
# And convert them over to where the object files should end up
ARCHASMOBJ = $(patsubst $(ARCHASM)/%,$(OBJDIR)/asm/%,$(ARCHASMSOURCES:.asm=.o))
ARCHASMTARGET = $(OBJDIR)/asm.o

TARGET = kern.$(ARCH).elf

ZIGTARGET = $(ZIGOUT)/kernel.elf
ZIGFLAGS = --prefix-exe-dir . -Darch=$(ARCH) -Drelease-safe -Dlog_level=debug -freference-trace=20

LDFLAGS = -T $(ARCHDIR)/linker.ld -nostdlib -g

all: $(TARGET)

$(TARGET): $(ZIGTARGET)
	@mv $(ZIGTARGET) $(TARGET)

$(ZIGTARGET): $(ARCHASMTARGET)
	@echo "Using zig build"
	$(ZZ) build $(ZIGFLAGS)

$(ARCHASMTARGET): $(ARCHASMOBJ)
	$(LD) $(LDFLAGS) -r -o $@ $^

$(OBJDIR)/asm/%.o: $(ARCHASM)/%.asm
	@#Make sure the directory exists
	@mkdir -p $(dir $@a)
	$(AS) $(ASFLAGS) $^ -o $@

.PHONY: clean
clean: zigclean
	@rm -rf $(OBJDIR) || echo "$(OBJDIR) already removed"
	@rm $(TARGET) || echo "$(TARGET) already removed"
.PHONY: zigclean
zigclean:
	@rm -rf zig-cache || echo "zig-cache already removed"
.PHONY: docclean
docclean:
	@rm -rf $(DOC) || echo "$(DOC) already removed"
.PHONY: distclean
distclean: clean docclean

.PHONY: doc
doc: export DOCDIR = $(DOC)
doc:
	doxygen Doxyfile
