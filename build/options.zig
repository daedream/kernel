const std = @import("std");
const Builder = std.build.Builder;
const builtin = @import("builtin");

// This option is here for documentation purposes on how to add an option to the kernel
pub const ExampleOption = enum {
    // The build option name that will be used on the command line
    pub const name = "example";
    // The name that will be used as the field in the build_options package
    pub const code_name = name;
    // The description that will show up in the help text
    pub const description = "An example build option";
    // The default value of the build option
    pub const default_value: @This() = .value3;
    // Put your options here
    value1,
    value2,
    value3,
    // Get a description of the value passed
    pub fn getDesc(val: @This()) []const u8 {
        return switch (val) {
            .value1 => "The first value",
            .value2 => "The second value",
            .value3 => "The third value",
        };
    }
    // Get a string representation of the choice. This can usually just be @tagName
    pub fn getString(val: @This()) []const u8 {
        return @tagName(val);
    }
    // You can add functions here that can be used in the program, but most
    // likely you won't want to do this.
};

// Arch is a special option and cannot be edited/removed!
pub const Arch = enum {
    pub const name = "arch";
    pub const code_name = name;
    pub const description = "The architecture to compile for";
    pub const default_value: @This() = .x86_64;
    x86_64,
    pub fn getDesc(arch: @This()) []const u8 {
        return switch (arch) {
            .x86_64 => "The x86_64 architecture",
        };
    }
    pub fn getString(arch: @This()) []const u8 {
        return @tagName(arch);
    }
    pub fn getTarget(arch: @This()) std.zig.CrossTarget {
        switch (arch) {
            .x86_64 => {
                const Feature = std.Target.Cpu.Feature;
                const Features = std.Target.x86.Feature;
                var disabled_features = Feature.Set.empty;
                var enabled_features = Feature.Set.empty;
                // We need software floating point calculations
                enabled_features.addFeature(@enumToInt(Features.soft_float));
                // We need to disable features that need to be initialized by the OS
                disabled_features.addFeature(@enumToInt(Features.mmx));
                disabled_features.addFeature(@enumToInt(Features.sse));
                disabled_features.addFeature(@enumToInt(Features.sse2));

                return std.zig.CrossTarget{
                    .cpu_arch = std.Target.Cpu.Arch.x86_64,
                    .os_tag = std.Target.Os.Tag.freestanding,
                    .abi = std.Target.Abi.none,
                    .cpu_features_sub = disabled_features,
                    .cpu_features_add = enabled_features,
                };
            },
        }
    }
};
pub const Bootloader = enum {
    pub const name = "bootloader";
    pub const code_name = name;
    pub const description = "The bootloader the kernel will support";
    pub const default_value: @This() = .limine;
    stivale,
    limine,
    pub fn getDesc(bootloader: @This()) []const u8 {
        return switch (bootloader) {
            .stivale => "A stivale2 compatible bootloader (Such as limine)",
            .limine => "A limine compatible bootloader",
        };
    }
    pub fn getString(bootloader: @This()) []const u8 {
        return @tagName(bootloader);
    }
};

pub const AllocatorType = enum {
    pub const name = "allocator_type";
    pub const code_name = name;
    pub const description = "The page allocator that will be used by the kernel";
    pub const default_value: @This() = .bitmap;
    bitmap,
    pub fn getDesc(a: @This()) []const u8 {
        return switch (a) {
            .bitmap => "A bitmap based allocator",
        };
    }
    pub fn getString(a: @This()) []const u8 {
        return @tagName(a);
    }
};

pub const LogLevel = enum {
    pub const name = "log_level";
    pub const code_name = name;
    pub const description = "The level of logging that this kernel will be compiled with";
    pub const default_value: @This() = .info;
    debug,
    info,
    warn,
    err,
    pub fn getDesc(val: @This()) []const u8 {
        return switch (val) {
            .debug => "Set log level to debug",
            .info => "Set log level to info",
            .warn => "Set log level to warn",
            .err => "Set log level to err",
        };
    }
    pub fn getString(val: @This()) []const u8 {
        return @tagName(val);
    }
};

// You will add your options here! When the builder calls the addOptions function, it'll
// iterate through this array and add these options
const options = .{
    Arch,
    Bootloader,
    // ExampleOption,
    AllocatorType,
    LogLevel,
};

pub fn getOption(b: *Builder, kernel_options: anytype, comptime option: type) option {
    const typeName = @typeName(option);
    inline for ([_][]const u8{ "name", "code_name", "description", "default_value" }) |decl| {
        if (!@hasDecl(option, decl)) {
            @compileError("Option " ++ typeName ++ " does not have a " ++ decl ++ " declared!");
        }
    }
    // Used to create descriptions in the build options
    const desc_padding: []const u8 = "                               ";
    comptime var options_desc: []const u8 = "\n " ++ desc_padding;
    inline for (@typeInfo(option).Enum.fields) |field| {
        const f = @field(option, field.name);
        options_desc = options_desc ++ field.name ++ ": " ++ comptime option.getDesc(f) ++ "\n " ++ desc_padding;
    }

    const option_val = b.option(option, option.name, option.description ++ "\n" ++ desc_padding ++ "Available options: " ++ options_desc) orelse option.default_value;
    kernel_options.addOption(option, option.code_name, option_val);
    return option_val;
}

// The builder needs the Arch option, so we'll return that from here
pub fn addOptions(b: *Builder, kernel_options: anytype) Arch {
    var arch: ?Arch = null;
    inline for (options) |option| {
        if (option == Arch) {
            arch = getOption(b, kernel_options, option);
        } else {
            _ = getOption(b, kernel_options, option);
        }
    }
    return arch.?;
}
