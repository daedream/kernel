const std = @import("std");
pub fn getBits(value: anytype, comptime start: comptime_int, comptime end: comptime_int) std.meta.Int(.unsigned, end - start) {
    const ValueType = @TypeOf(value);
    const ReturnType = std.meta.Int(.unsigned, end - start);

    // Do some error checking
    comptime {
        if (end <= start) {
            @compileError("End cannot be less than or equal to start!");
        }
        switch (@typeInfo(ValueType)) {
            .Int => |info| {
                if (info.signedness != .unsigned) {
                    @compileError("Value must be an unsigned integer. Found " ++ @typeName(ValueType));
                }
                if (start > @bitSizeOf(ValueType)) {
                    @compileError("Start is out of bounds.");
                }
                if (end > @bitSizeOf(ValueType)) {
                    @compileError("End is out of bounds.");
                }
            },
            else => {
                @compileError("Value must be an unsigned integer. Found " ++ @typeName(ValueType));
            },
        }
    }

    return @truncate(ReturnType, value >> start);
}

pub fn signExtend(value: anytype, comptime end: comptime_int) @TypeOf(value) {
    const ValueType = @TypeOf(value);

    comptime {
        switch (@typeInfo(ValueType)) {
            .Int => |info| {
                if (info.signedness != .unsigned) {
                    @compileError("Value must be an unsigned integer. Found " ++ @typeName(ValueType));
                }
                if (end >= @bitSizeOf(ValueType)) {
                    @compileError("End is out of bounds.");
                }
            },
            else => {
                @compileError("Value must be an unsigned integer. Found " ++ @typeName(ValueType));
            },
        }
    }
    const truncate_shift = @bitSizeOf(ValueType) - end;
    const SignedVersion = std.meta.Int(.signed, @bitSizeOf(ValueType));
    return @bitCast(ValueType, @bitCast(SignedVersion, value << truncate_shift) >> truncate_shift);
}
