const std = @import("std");

pub fn formatFlagStruct(value: anytype, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
    _ = fmt;
    comptime var ValType = @TypeOf(value);
    switch (@typeInfo(ValType)) {
        .Struct => |info| {
            try writer.writeAll(@typeName(ValType));
            try writer.writeAll("{");
            var first: bool = true;
            inline for (info.fields) |field| {
                if (!std.mem.startsWith(u8, field.name, "reserved")) {
                    if (field.type == bool) {
                        if (@field(value, field.name) == true) {
                            if (first) {
                                try writer.writeAll(" .");
                                first = false;
                            } else {
                                try writer.writeAll(", .");
                            }
                            try writer.writeAll(field.name);
                        }
                    } else {
                        if (first) {
                            try writer.writeAll(" .");
                            first = false;
                        } else {
                            try writer.writeAll(", .");
                        }
                        try writer.writeAll(field.name);
                        try writer.writeAll(" = ");
                        try std.fmt.formatType(@field(value, field.name), "any", options, writer, std.fmt.default_max_depth - 1);
                    }
                }
            }
            try writer.writeAll(" }");
        },
        else => {
            unreachable;
        },
    }
}
