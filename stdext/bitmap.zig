const usize_size = @bitSizeOf(usize);
const std = @import("std");

pub fn bitCountToBackingSize(size: anytype) usize {
    return @divFloor(size, usize_size) + @boolToInt(size % usize_size != 0);
}

pub const Bitmap = struct {
    backing_array: []usize,

    pub const BitmapError = error{
        OutOfRange,
        InvalidRange,
    };
    pub fn capacity(self: @This()) usize {
        return self.backing_array.len * usize_size;
    }
    fn getBackingIndex(index: usize) usize {
        return @divFloor(index, usize_size);
    }
    fn getBackingMask(index: usize) usize {
        return @as(usize, 1) <<| index % usize_size;
    }
    pub fn set(self: *@This(), index: usize) BitmapError!void {
        if (index >= self.capacity()) return BitmapError.OutOfRange;
        self.backing_array[getBackingIndex(index)] |= getBackingMask(index);
    }
    pub fn unset(self: *@This(), index: usize) BitmapError!void {
        if (index >= self.capacity()) return BitmapError.OutOfRange;
        self.backing_array[getBackingIndex(index)] &= ~getBackingMask(index);
    }
    pub fn get(self: *@This(), index: usize) BitmapError!bool {
        if (index >= self.capacity()) return BitmapError.OutOfRange;
        return (self.backing_array[getBackingIndex(index)] & getBackingMask(index)) != 0;
    }
    pub fn setToValue(self: *@This(), index: usize, value: bool) BitmapError!void {
        if (index >= self.capacity()) return BitmapError.OutOfRange;
        if (value) {
            try self.set(index);
        } else {
            try self.unset(index);
        }
    }
    pub fn setRange(self: *@This(), start: usize, end: usize) BitmapError!void {
        if (end <= start) return BitmapError.InvalidRange;
        if (end >= self.capacity() or start >= self.capacity()) return BitmapError.OutOfRange;
        var i: usize = start;
        while (i <= end) : (i += 1) {
            try self.set(i);
        }
    }
    pub fn setRangeClamped(self: *@This(), unclamped_start: usize, unclamped_end: usize) BitmapError!void {
        var end = std.math.clamp(unclamped_end, 0, self.capacity());
        var start = std.math.clamp(unclamped_start, 0, self.capacity());
        if (start == end) return;
        try self.setRange(start, end);
    }
    pub fn unsetRange(self: *@This(), start: usize, end: usize) BitmapError!void {
        if (end <= start) return BitmapError.InvalidRange;
        if (end >= self.capacity() or start >= self.capacity()) return BitmapError.OutOfRange;
        var i: usize = start;
        while (i <= end) : (i += 1) {
            try self.unset(i);
        }
    }
    pub fn unsetRangeClamped(self: *@This(), unclamped_start: usize, unclamped_end: usize) BitmapError!void {
        var end = std.math.clamp(unclamped_end, 0, self.capacity());
        var start = std.math.clamp(unclamped_start, 0, self.capacity());
        if (start == end) return;
        try self.unsetRange(start, end);
    }
    pub fn setRangeToValue(self: *@This(), start: usize, end: usize, value: bool) BitmapError!void {
        if (value) {
            try self.setRange(start, end);
        } else {
            try self.unsetRange(start, end);
        }
    }

    // Check if there are any values within the range that share the value
    pub fn checkRange(self: *@This(), start: usize, end: usize, value: bool) BitmapError!bool {
        if (end <= start) return BitmapError.InvalidRange;
        if (end >= self.capacity() or start >= self.capacity()) return BitmapError.OutOfRange;
        var i: usize = start;
        while (i <= end) : (i += 1) {
            if (self.get(i) catch unreachable == value)
                return true;
        }
        return false;
    }

    pub fn unsetAll(self: *@This()) void {
        self.unsetRange(0, self.capacity() - 1) catch {};
    }

    pub fn setAll(self: *@This()) void {
        self.setRange(0, self.capacity() - 1) catch {};
    }

    pub fn findFirstSet(self: *@This()) ?usize {
        return self.findFirstValue(true);
    }
    pub fn findFirstUnset(self: *@This()) ?usize {
        return self.findFirstValue(false);
    }
    pub fn findFirstValue(self: *@This(), value: bool) ?usize {
        var i: usize = 0;
        while (i < self.capacity()) : (i += 1) {
            if (self.get(i) catch unreachable == value)
                return i;
        }
        return null;
    }
    pub fn findLastSet(self: *@This()) ?usize {
        return self.findLastValue(true);
    }
    pub fn findLastUnset(self: *@This()) ?usize {
        return self.findLastValue(false);
    }
    pub fn findLastValue(self: *@This(), value: bool) ?usize {
        var i: usize = self.capacity() - 1;
        while (i >= 0) : (i -= 1) {
            if (self.get(i) catch unreachable == value)
                return i;
        }
        return null;
    }
    pub fn findFirstRangeFree(self: *@This(), size: usize) ?usize {
        return self.findFirstRangeValue(size, false);
    }
    pub fn findFirstRangeUsed(self: *@This(), size: usize) ?usize {
        return self.findFirstRangeValue(size, true);
    }
    pub fn findFirstRangeValue(self: *@This(), size: usize, value: bool) ?usize {
        var i: usize = 0;
        var cur_len: usize = 0;
        var first_value: ?usize = 0;
        while (i < self.capacity()) : (i += 1) {
            if (self.get(i) catch unreachable == value) {
                if (first_value) |_| {} else {
                    first_value = i;
                }
                cur_len += 1;
                if (cur_len == size)
                    return first_value;
            } else {
                first_value = null;
            }
        }
        return first_value;
    }

    pub fn findLastRangeValue(self: *@This(), size: usize, value: bool) ?usize {
        var i: usize = self.capacity() - 1;
        var cur_len: usize = 0;
        var first_value: ?usize = 0;
        while (i >= 0) : (i -= 1) {
            if (self.get(i) catch unreachable == value) {
                if (first_value) |_| {} else {
                    first_value = i;
                }
                cur_len += 1;
                if (cur_len == size)
                    return first_value;
            } else {
                first_value = null;
            }
        }
        return first_value;
    }

    pub fn findLastRangeFree(self: *@This(), size: usize) ?usize {
        return self.findLastRangeValue(size, false);
    }
    pub fn findLastRangeUsed(self: *@This(), size: usize) ?usize {
        return self.findLastRangeValue(size, true);
    }
};
