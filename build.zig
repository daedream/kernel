const std = @import("std");
const Builder = std.build.Builder;
const builtin = @import("builtin");
const options = @import("build/options.zig");

pub fn build(b: *Builder) !void {

    //========
    //Kernel executable setup
    //========

    const kernel = b.addExecutable("kernel.elf", "src/main.zig");
    const kernel_options = b.addOptions();

    kernel.addOptions("build_options", kernel_options);

    //===========
    //We'll set up our options here
    //===========
    //
    // The code is a bit kludg-y here, but we need the arch option, so the
    // function to initialize them returns it
    const arch = options.addOptions(b, kernel_options);

    //========
    //Start setting up our kernel
    //compilation
    //========

    const mode = b.standardReleaseOptions();

    // Set up everything we need for cross compilation before we
    // even start adding source files and stuff
    const arch_string = arch.getString();
    const arch_dir = try std.fs.path.join(b.allocator, &[_][]const u8{ "src", "arch", arch_string });
    const linker_script = try std.fs.path.join(b.allocator, &[_][]const u8{ arch_dir, "linker.ld" });

    //========
    //Packages setup
    //========
    //
    //List of packages:
    //stdext

    const stdext_pkg = stdext_pkg: {
        const stdext_source_file = std.build.FileSource.relative("stdext/main.zig");
        break :stdext_pkg std.build.Pkg{
            .name = "stdext",
            .source = stdext_source_file,
            .dependencies = &[_]std.build.Pkg{},
        };
    };

    //========
    // Finish Kernel executable setup
    //========

    // Add our packages
    kernel.addPackage(stdext_pkg);

    // Add the assembly object file
    // This is done in the makefile
    kernel.addObjectFile("zig-out/obj/asm.o");

    kernel.setTarget(arch.getTarget());
    //kernel.link_gc_sections = false;
    kernel.link_emit_relocs = true;
    kernel.setBuildMode(mode);
    kernel.setLinkerScriptPath(std.build.FileSource.relative(linker_script));
    kernel.code_model = .kernel;
    //kernel.emit_docs = .{ .emit = {} };
    kernel.install();
}
